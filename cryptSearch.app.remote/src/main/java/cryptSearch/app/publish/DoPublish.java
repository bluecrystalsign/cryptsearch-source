package cryptSearch.app.publish;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cryptSearch.lib.publish.remote.factory.PublishRemoteFactory;
import cryptSearch.lib.publish.remote.factory.PublishRemoteFactoryImpl;
import cryptSearch.lib.publish.remote.worker.PublishRemoteWorker;
import cryptSearch.lib.publish.remote.worker.PublishRemoteWorkerImpl;
import cryptSearch.service.commons.persist.TokenListStore;
import cryptSearch.service.commons.util.Timelapse;

public class DoPublish {

	public DoPublish() {
		super();
	}

	public static void main(String[] args) {
		System.out.println(Messages.getString("DoPublish.18")); //$NON-NLS-1$
		start(args);
		System.out.println(Messages.getString("DoPublish.19")); //$NON-NLS-1$

	}

	private static void start(String[] args) {
		if (args.length != 0) {
			List<String> behaviour = null;
			if (args.length > 1) {
				behaviour = Arrays.asList(Arrays.copyOfRange(args, 0, args.length));
			}
			try {
				String string = Messages.getString("DoPublish.20");
				String string2 = Messages.getString("DoPublish.21");
				if (
						behaviour != null && (behaviour.contains(string)
								|| behaviour.contains(string2) )
						) { //$NON-NLS-1$ //$NON-NLS-2$
					new DoPublish().update(args);
				} else if(behaviour != null && (behaviour.contains("--rebuild"))) {
					System.out.println("Executing REBUILD");
					PublishRemoteFactory prf = PublishRemoteFactoryImpl.getInstance();
					PublishRemoteWorker prw = new PublishRemoteWorkerImpl(prf);
					String indexName = args[args.length-1];
					prw.rebuildTokenBySize(indexName);					
				} else {
					new DoPublish().create(args);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			printUsage(args);
		}
	}

	private void update(String[] args) throws Exception {
		System.out.println(Messages.getString("DoPublish.22")); //$NON-NLS-1$
		Timelapse totalTime = new Timelapse();
//		if (invalidParemsUpdate(args)) {
//			printUsage(args);
//			return;
//		}

		String indexName = args[1];

//		Map<String, Set<String>> tokensToIndex = null;

		PublishRemoteFactory prf = PublishRemoteFactoryImpl.getInstance();
		PublishRemoteWorker prw = new PublishRemoteWorkerImpl(prf);
		prw.initIndex(indexName, false);
		TokenListStore tokenList = prf.getTokenList(Messages.getString("DoPublish.25")); //$NON-NLS-1$
		Map<String, Set<String>> tokensToIndex = loadTokensToIndex(indexName, tokenList);

		for (String contentRefence : tokensToIndex.keySet()) {
			prw.updateIndex(contentRefence, tokensToIndex.get(contentRefence));
		}

		prw.finish();

		totalTime.print(Messages.getString("DoPublish.23")); //$NON-NLS-1$

	}

	private void create(String[] args) throws Exception {
		Timelapse totalTime = new Timelapse();
//		if (invalidParamsCreate(args)) {
//			printUsage(args);
//			return;
//		}
		String indexName = args[0];

		System.out.println(Messages.getString("DoPublish.24")+indexName); //$NON-NLS-1$

		PublishRemoteFactory prf = PublishRemoteFactoryImpl.getInstance();
		TokenListStore tokenList = prf.getTokenList(Messages.getString("DoPublish.25")); //$NON-NLS-1$
		Map<String, Set<String>> tokensToIndex = loadTokensToIndex(indexName, tokenList);


		PublishRemoteWorker prw = new PublishRemoteWorkerImpl(prf);
		prw.initIndex(indexName, true);
		for (String contentRefence : tokensToIndex.keySet()) {
			prw.addDocToIndex(contentRefence, tokensToIndex.get(contentRefence));
		}

		prw.finish();
		tokenList.deleteRemote(indexName);

		totalTime.print(Messages.getString("DoPublish.26")); //$NON-NLS-1$

	}

	private Map<String, Set<String>> loadTokensToIndex(String indexName, TokenListStore tokenList)
			throws FileNotFoundException, IOException {
		Map<String, Object> indexedTokens = tokenList.loadRemote(indexName);
		
		Map<String, Set<String>> tokensToIndex = new HashMap<>();
		for(String next : indexedTokens.keySet()) {
			ArrayList object = (ArrayList) indexedTokens.get(next);
			Set<String> value = new HashSet<String>();
			value.addAll(object);
			tokensToIndex.put(next, value);
		}
		return tokensToIndex;
	}

	

	private boolean invalidParamsCreate(String[] args) {
		if (args.length >= 2) {
			File f = new File(args[args.length - 1]);
			return !(f.exists() && f.isDirectory() && f.canRead());
		}
		return true;
	}

	private boolean invalidParemsUpdate(String[] args) {
		if (args.length >= 3) {
			File f = new File(args[args.length - 1]);
			return !(f.exists() && f.isDirectory() && f.canRead());
		}
		return true;
	}

	private static void printUsage(String[] args) {
		System.out.println(Messages.getString("DoPublish.27")); //$NON-NLS-1$
		System.out.println(
				Messages.getString("DoPublish.28")); //$NON-NLS-1$
		System.out.println(
				Messages.getString("DoPublish.29")); //$NON-NLS-1$
		for (String next : args) {
			System.out.println(Messages.getString("DoPublish.30") + next); //$NON-NLS-1$
		}

	}

}
