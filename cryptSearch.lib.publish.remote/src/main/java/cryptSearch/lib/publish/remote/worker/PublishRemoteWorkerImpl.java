package cryptSearch.lib.publish.remote.worker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.MMapDirectory;

import cryptSearch.lib.publish.remote.factory.PublishRemoteFactory;
import cryptSearch.lib.publish.remote.util.RemoteIndexPublisher;


public class PublishRemoteWorkerImpl implements PublishRemoteWorker {
	private  String indexName;	
	private  RemoteIndexPublisher index;
	private PublishRemoteFactory prf;
	
	public PublishRemoteWorkerImpl(PublishRemoteFactory prf) {
		super();
		this.prf = prf;
	}

	/* (non-Javadoc)
	 * @see cryptSearch.lib.publish.remote.PublishRemoteWorker#getIndexName()
	 */
	public  String getIndexName() {
		return indexName;
	}

	/* (non-Javadoc)
	 * @see cryptSearch.lib.publish.remote.PublishRemoteWorker#setIndexName(java.lang.String)
	 */
	public  void initIndex(String indexName, boolean recreate) throws Exception {
		this.indexName = indexName;
		index = prf.getIndexer(indexName, prf.getRemoteBucketManager(), recreate);
			index.create();
	}
	
	/* (non-Javadoc)
	 * @see cryptSearch.lib.publish.remote.PublishRemoteWorker#addDocToIndex(java.lang.String, java.util.Set, java.lang.String)
	 */
	public void addDocToIndex(String url, Set<String> encSet) throws Exception {
		index.addDocToIndex(url, encSet);
	}
	/* (non-Javadoc)
	 * @see cryptSearch.lib.publish.remote.PublishRemoteWorker#finish()
	 */
	public void finish() throws IOException {
		index.finish();
	}

	public void updateIndex(String url, Set<String> encSet)  throws Exception {
		index.addDocToIndex(url, encSet);
		
	}

	@Override
	public void rebuildTokenBySize(String indexName) throws Exception {
		Directory index = MMapDirectory.open(prf.getRemoteBucketManager().getIndexPath(indexName).toPath());
		IndexReader reader = DirectoryReader.open(index);
		rebuild(reader);
	}
	
	private void rebuild(IndexReader reader) throws IOException {
		
		Map<Integer, Integer> countByToken = new HashMap<>();
		int maxDoc = reader.maxDoc();
		Set<String> unrepeater = new HashSet<>();
		System.out.println("Showing "+maxDoc+ " documents");
		for (int i = 0; i < maxDoc; i++) {
			List<String> ret2 = new ArrayList<String>();
			Document doc = reader.document(i);

			String content = doc.get("content");
			String[] terms = content.split(" ");
			for(String term : terms ) {
				unrepeater.add(term);

			}
		}
		System.out.println("-- rebuild");
		for(String next : unrepeater) {
			int len = next.length()/2;
			if(!countByToken.containsKey(len)) {
				countByToken.put(len, 1);
			} else {
				countByToken.put(len, countByToken.get(len)+1);
			}		
		}
		
		int maxTokens = 0;
		for(Integer next : countByToken.keySet()) {
			System.out.println(next + "->" +countByToken.get(next));
			maxTokens += countByToken.get(next);
		}
		System.out.println("maxTokens: " +maxTokens);
	}
	

}
