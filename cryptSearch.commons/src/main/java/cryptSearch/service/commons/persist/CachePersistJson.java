package cryptSearch.service.commons.persist;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;

import cryptSearch.service.commons.content.LocalBucketManager;
import cryptSearch.service.commons.domain.TermsCache;

public class CachePersistJson implements CachePersist {
	private static final int MAX_CACHE_FILES = 100 * 1000;
	private LocalBucketManager bucket;

	
	
	public CachePersistJson(LocalBucketManager bucket) {
		super();
		this.bucket = bucket;
	}

	public void init(LocalBucketManager bucket, boolean delete) {
		this.bucket = bucket;
		if (delete) {
			this.bucket.clearCache();
		}
	}

	public List<TermsCache>[] loadFromCache(String indexName) throws Exception {
		int cacheCount = this.bucket.getNextCacheFile(indexName);

		List<TermsCache>[] retTerms = (List<TermsCache>[]) new ArrayList[cacheCount];

		for (int i = 0; i < cacheCount; i++) {
			File cacheFile = bucket.getCacheFile(indexName, i);
			System.err.println("Loading CACHE: "+cacheFile.getAbsolutePath());
			InputStream is = new FileInputStream(cacheFile);
			byte[] b = new byte[is.available()];
			is.read(b);
			List<TermsCache> unmarshall = unmarshall(new String(b));
			is.close();
			retTerms[i] = unmarshall;
		}
		return retTerms;
	}

	public void persistCache(String indexName,
			Map<Integer, Set<String>> sumTokens) throws Exception {
		List<TermsCache> cache = new ArrayList<TermsCache>();
		for (Integer nextInt : sumTokens.keySet()) {
			cache.add(new TermsCache(nextInt, sumTokens.get(nextInt).size()));
		}
		File cacheFile = bucket.createCacheFile(indexName);
		System.err.println("Persisting CACHE: "+cacheFile.getAbsolutePath());
		writeToFile(cache, cacheFile);
	}


	private String marshall(List<TermsCache> ks) {
		JSONArray ja = new JSONArray(ks);
		return ja.toString();

	}

	private List<TermsCache> unmarshall(String s) {
		JSONArray ja = new JSONArray(s);
		List<TermsCache> list = new ArrayList<>();
		for(int i =0; i < ja.length(); i++) {
			JSONObject jo = ja.getJSONObject(i);
			TermsCache termsCache = new Gson().fromJson(jo.toString(), TermsCache.class);
			list.add(termsCache);
		}
		return list;
	}

	private void writeToFile(List<TermsCache> term, File f) throws Exception {
		byte[] bytes = marshall(term).getBytes();

		this.bucket.writeToFile(bytes, f);

	}

	@Override
	public void initLocalBucketManager(boolean delete) {
		if (delete) {
			this.bucket.clearCache();
		}
	}

}
