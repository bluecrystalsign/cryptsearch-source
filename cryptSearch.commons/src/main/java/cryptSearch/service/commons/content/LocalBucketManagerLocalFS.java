package cryptSearch.service.commons.content;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import cryptSearch.service.commons.domain.ContentRef;

public class LocalBucketManagerLocalFS implements LocalBucketManager {

	private static final int MAX_FRAGMENTS = 10*1000;
	private final String BASE_PATH = System.getProperty("user.home") + File.separator + "cryptsearch" + File.separator +  "local"; //$NON-NLS-1$
	private final String KS_PATH = "keyStore"; //$NON-NLS-1$
	private final String CACHE_PATH = "cache"; //$NON-NLS-1$
	private final String REF_PATH = "refByName"; //$NON-NLS-1$
	private final String CLEAR_PATH = "clear"; //$NON-NLS-1$
	private final String TOKEN_LIST_PATH = "encryptedTokenByDocument"; //$NON-NLS-1$
	private final String TEMP_PATH = System.getProperty("user.home") + File.separator + "cryptsearch" + File.separator +  "temp"; //$NON-NLS-1$
	private final String EXTENSION = ".json";

	private static LocalBucketManager instance;
	public static LocalBucketManager getInstance() {
		if(instance == null) {
			instance = new LocalBucketManagerLocalFS();
		}
		return instance;
	}
	
	private LocalBucketManagerLocalFS() {
		createDirs();
	}
	private void createDirs() {

		if (!getKsPath().exists()) {
			getKsPath().mkdirs();
		}
		if (!getCachePath().exists()) {
			getCachePath().mkdirs();
		}

		if (!getRefByNamePath().exists()) {
			getRefByNamePath().mkdirs();
		}

		if (!getClearPath().exists()) {
			getClearPath().mkdirs();
		}
		if (!getTokenListPath().exists()) {
			getTokenListPath().mkdirs();
		}
	}
	
	@Override
	public File getTokenListFile(String filePath) {
		return new File(getTokenListPath() + File.separator
				+ filePath+ EXTENSION);
	}	


	public File getKsFile(String filePath) {
		return new File(getKsPath() + File.separator
				+ filePath);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cryptSearch.service.commons.content.LocalBucketManager#getCachePath()
	 */
	public void clearCache() {
		File f = getCachePath();
		deleteDirectory(f);
		createDirs();
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cryptSearch.service.commons.content.LocalBucketManager#getCacheFile(java
	 * .lang.String)
	 */
	public File createCacheFile(String filePath) {
		int next = getNextCacheFile(filePath);
		File basePath = new File(getCachePath()
				+ File.separator + filePath);
		File nextFile =  new File(basePath.getAbsoluteFile() + File.separator + String.valueOf(next) + EXTENSION);
		
		return nextFile; //$NON-NLS-1$
	}

	public int getNextCacheFile(String filePath) {
		File basePath = new File(getCachePath()	+ File.separator + filePath);
		if(!basePath.exists()){
			basePath.mkdir();
		}
		int i = 0;
		File nextFile =  new File(basePath.getAbsoluteFile() + File.separator + String.valueOf(0) +  EXTENSION);
		if(basePath.isDirectory()){
			for(; i < MAX_FRAGMENTS; i++){
				nextFile =  new File(basePath.getAbsoluteFile() + File.separator + String.valueOf(i) + EXTENSION);
				if(!nextFile.exists()){
					break;
				}
			}
		}
		return i; //$NON-NLS-1$
	}

	public File createCollectorFile(String filePath) {
		int next = getNextCacheFile(filePath);
		File basePath = new File(getCachePath()
				+ File.separator + filePath);
		File nextFile =  new File(basePath.getAbsoluteFile() + File.separator + "collector.txt");
		
		return nextFile; //$NON-NLS-1$
	}

	
	public File getCacheFile(String filePath, int i) {
		File basePath = new File(getCachePath() + File.separator + filePath);
		if(!basePath.exists()){
			basePath.mkdir();
		}
		
		File nextFile =  new File(basePath.getAbsoluteFile() + File.separator + String.valueOf(i) +  EXTENSION);
		return nextFile; //$NON-NLS-1$
	}

	public File getRefByNameFile(String filePath) {
		return new File(getRefByNamePath() + File.separator
				+ filePath + EXTENSION); //$NON-NLS-1$
	}
	public File getClearFile(String filePath) {
		return new File(getClearPath()
				+ File.separator + filePath); //$NON-NLS-1$
	}


	public boolean deleteClearContent() {
		if (getClearPath().exists()) {
			File[] files = getClearPath().listFiles();
			boolean isDel = true;
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					isDel = deleteDirectory(files[i]);
				} else {
					isDel = files[i].delete();
				}
				if (!isDel) {
					return false;
				}
			}
		}
		return (true);
	}


	public void saveClearContent(ContentRef contentRef, byte[] clearContent)
			throws Exception {
		String destOrig = contentRef.getName();
			File destFile = new File(destOrig);
			File dest = getClearFile(destFile.getName());
			System.out.println("\t" + dest.getAbsolutePath());
			saveContent(dest, clearContent);
	}



//	public Map<Integer, ContentRef> loadRefByName(String indexName)
//			throws Exception {
//		RefByNamePersist xs = new RefByNamePersistJson();
////		xs.init(this);
//		Map<Integer, ContentRef> refByName = xs.load(indexName);
//		return refByName;
//	}

	public byte[] loadContent(String next) throws FileNotFoundException,
			IOException {
		System.err.println("Loading CLEAR CONTENT: " +next);

		InputStream is = new FileInputStream(next);
		byte[] content = new byte[is.available()];
		is.read(content);
		is.close();
		return content;
	}

	public void writeToFile(byte[] bytes, File f) throws Exception {
			OutputStream os = new FileOutputStream(f);
			os.write(bytes);
			os.close();
	}

	public void createFolder(String path){
		File f = new File(path);
		f.mkdirs();
	}

	private File getKsPath() {
		return new File(BASE_PATH + File.separator + KS_PATH);
	}
	private File getCachePath() {
		return new File(BASE_PATH + File.separator + CACHE_PATH);
	}
	private File getRefByNamePath() {
		return new File(BASE_PATH + File.separator + REF_PATH);
	}
	private File getClearPath() {
		return new File(BASE_PATH + File.separator + CLEAR_PATH);
	}
	
	private File getTokenListPath() {
		return new File(TEMP_PATH + File.separator + TOKEN_LIST_PATH);
	}

	private void saveContent(File dest, byte[] clearContent) throws Exception {
		File ksPath = dest.getParentFile();
		ksPath.mkdirs();

		OutputStream os = new FileOutputStream(dest);
		os.write(clearContent);
		os.close();
	}

	private boolean deleteDirectory(File path) {
		if (path.exists()) {
			File[] files = path.listFiles();
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					deleteDirectory(files[i]);
				} else {
					files[i].delete();
				}
			}
		}
		return (path.delete());
	}

}
