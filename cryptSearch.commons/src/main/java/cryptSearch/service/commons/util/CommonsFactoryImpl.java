package cryptSearch.service.commons.util;

import cryptSearch.service.commons.content.LocalBucketManager;
import cryptSearch.service.commons.content.LocalBucketManagerLocalFS;
import cryptSearch.service.commons.content.RemoteBucketManager;
import cryptSearch.service.commons.content.RemoteBucketManagerLocalFS;
import cryptSearch.service.commons.crypto.CryptoService;
import cryptSearch.service.commons.crypto.CryptoServiceAes;
import cryptSearch.service.commons.persist.CachePersist;
import cryptSearch.service.commons.persist.CachePersistJson;
import cryptSearch.service.commons.persist.RefByNamePersist;
import cryptSearch.service.commons.persist.RefByNamePersistJson;
import cryptSearch.service.commons.persist.TokenListStore;
import cryptSearch.service.commons.persist.TokenListJson;

public class CommonsFactoryImpl implements CommonsFactory {
	private static CommonsFactory factory;
	private static RefByNamePersistJson refByNamePersistJson;
	private static CachePersistJson cachePersistJson;
	private static TokenListJson tokenListJson;
	private static CryptoServiceAes cryptoServiceAes;
	
	private CommonsFactoryImpl() {
		super();
	}

	public static CommonsFactory getInstance(){
		if(factory == null){
			factory = new CommonsFactoryImpl();
		}
		return factory;
	}
	
	@Override
	public RemoteBucketManager getRemoteBucketManager() {
		return RemoteBucketManagerLocalFS.getInstance();
	}

	@Override
	public LocalBucketManager getLocalBucketManager() {
		return LocalBucketManagerLocalFS.getInstance();
	}
	
	@Override
	public CryptoService getCryptoService() {
		if(cryptoServiceAes == null){
			cryptoServiceAes = new CryptoServiceAes(RemoteBucketManagerLocalFS.getInstance(), LocalBucketManagerLocalFS.getInstance());
		}
		return cryptoServiceAes;
	}

	@Override
	public RefByNamePersist getRefByNamePersist() 
	{
		if(refByNamePersistJson == null){
			refByNamePersistJson = new RefByNamePersistJson(LocalBucketManagerLocalFS.getInstance());
		}
		return refByNamePersistJson;
	}
	

	@Override
	public CachePersist getCachePersist() 
	{ 
		if(cachePersistJson == null){
			cachePersistJson = new CachePersistJson(LocalBucketManagerLocalFS.getInstance());
		}
		return cachePersistJson;
	}
	
	@Override
	public TokenListStore getTokenList(String filePath) {
		if(tokenListJson == null){
			tokenListJson = new TokenListJson();
		}
		return tokenListJson;
	}

}
