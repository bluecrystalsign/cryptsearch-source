package cryptSearch.service.commons.persist;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.Set;

import org.json.JSONObject;

import cryptSearch.service.commons.content.LocalBucketManager;
import cryptSearch.service.commons.content.RemoteBucketManager;

public class TokenListJson implements TokenListStore {

	private LocalBucketManager localBucketManager;
	private RemoteBucketManager remoteBucketManager;
	
	@Override
	public void init(LocalBucketManager bucket) {
		this.localBucketManager = bucket;
	}
	
	@Override
	public void init(RemoteBucketManager bucket) {
		this.remoteBucketManager = bucket;
	}
	
	@Override
	public void persistLocal(String indexName, Map<String, Set<String>> tokensToIndex) throws FileNotFoundException, IOException {
		File dataFile = localBucketManager.getTokenListFile(indexName);
		if(dataFile.exists()) {
			dataFile.delete();
		}
		System.err.println("Persisting Local Encrypted Token By Documnent: " +dataFile);
		
		JSONObject jo = new JSONObject(tokensToIndex);
		
		OutputStream os = new FileOutputStream(dataFile);
		String joAsStr = jo.toString();
		os.write(joAsStr.getBytes());
		os.close();
	}

	@Override
	public Map<String, Object> loadLocal(String indexName) throws FileNotFoundException, IOException {
		File dataFile = localBucketManager.getTokenListFile(indexName);
		System.err.println("Loading Local TOKEN LIST: " +dataFile);
		
		InputStream os = new FileInputStream(dataFile);

		byte[] b = new byte[os.available()];
		os.read(b);
		JSONObject jo = new JSONObject(new String(b));
		Map<String, Object> tokens = jo.toMap();
		return tokens;
	}

	
	@Override
	public Map<String, Object> loadRemote(String indexName) throws FileNotFoundException, IOException {
		File dataFile = remoteBucketManager.getTokenListFile(indexName);
		System.err.println("Loading Remote TOKEN LIST: " +dataFile);
		
		InputStream os = new FileInputStream(dataFile);

		byte[] b = new byte[os.available()];
		os.read(b);
		JSONObject jo = new JSONObject(new String(b));
		Map<String, Object> tokens = jo.toMap();
		return tokens;
	}
	
	@Override
	public void deleteRemote(String indexName) throws FileNotFoundException, IOException {
		File dataFile = remoteBucketManager.getTokenListFile(indexName);
		System.err.println("Erasing Remote TOKEN LIST: " +dataFile);
	}
}
