package cryptSearch.lib.search.local.worker;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.crypto.NoSuchPaddingException;

import com.thoughtworks.xstream.core.util.OrderRetainingMap;

import cryptSearch.lib.search.local.factory.SearchLocalFactory;
import cryptSearch.lib.search.local.util.RegexAnalysis;
import cryptSearch.lib.search.local.util.RegexTips;
import cryptSearch.lib.search.local.util.SearchCryptoService;
import cryptSearch.service.commons.content.LocalBucketManager;
import cryptSearch.service.commons.crypto.CipherKey;
import cryptSearch.service.commons.domain.TermsCache;
import cryptSearch.service.commons.persist.CachePersist;
import cryptSearch.service.commons.util.CacheHandler;
import cryptSearch.service.commons.util.Converter;

public class SearchLocalWorkerImpl implements SearchLocalWorker {
	private static final int NO_WC = 0;
	private static final int STARTS_WITH = 1;
	// private List<TermsCache>[] cache;
	private CacheHandler cache;
	private SearchCryptoService cryptoService;
	private CachePersist persist;
	private RegexAnalysis regexAnalysis; 

	public SearchLocalWorkerImpl(SearchLocalFactory slFactory) {
		super();
		cryptoService = slFactory.getCryptoService();
		LocalBucketManager localBucketManager = slFactory
				.getLocalBucketManager();
		persist = slFactory.getCachePersist();
		persist.init(localBucketManager, false);
		cache = slFactory.getCacheHandler(false);
		regexAnalysis = slFactory.getRegexAnalysis();
		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cryptSearch.lib.search.local.LocalSearchWorker#encryptTerm(java.lang.
	 * String, java.lang.String, java.lang.String)
	 */
	public Set<String> encryptTerm(String term, String indexName, Boolean regex)
			throws Exception, NoSuchAlgorithmException,
			NoSuchProviderException, NoSuchPaddingException {
		Set<String> encryptedTerm;
		loadCache(indexName);
		CipherKey cipherKey = cryptoService.loadStreamKeyStore(indexName);

		encryptedTerm = encryptTerm(term, cipherKey, regex);
		return encryptedTerm;
	}

	private void loadCache(String indexName) throws Exception {
		cache.loadFromCache(indexName);
	}

	private Set<String> encryptTerm(String term, CipherKey ck, Boolean regex) throws Exception {
		
//		Any Unicode characters may be used in the pattern, but certain characters are reserved and must be escaped. The standard reserved characters are:
//
//			. ? + * | { } [ ] ( ) " \
//
//			If you enable optional features (see below) then these characters may also be reserved:
//
//			@ &   ~
		
		System.out.println("---Regex: "+regex);
		RegexTips regexTips = regexAnalysis.analiseRegex(term);
		
		String checkTerm = term;
		int maxLengthTerm = term.length();
		int minLengthTerm = term.length();

		SortedSet<Integer> sorted = new TreeSet<Integer>();
		
		Set<String> ret = new HashSet<String>();
		int prefixLength = 0;
		int cacheCount = cache.getCount();
		for (int counter = 0; counter < cacheCount; counter++) {
//			System.out.println("encryptTerm: "+counter);
			int operation = NO_WC;
			cache.addSorted(counter, sorted);

			if (term.endsWith("*")) {
				checkTerm = term.substring(0, term.length() - 1);
				minLengthTerm = term.length() - 1;
				operation = STARTS_WITH;
				maxLengthTerm = sorted.last();
			} else if (term.endsWith("?")) {
				checkTerm = term.substring(0, term.length() - 1);
				operation = STARTS_WITH;
			}

			switch (operation) {
			case NO_WC:
				if (!cache.isCacheLength(checkTerm.length(), counter)) {
					return null;
				}
				break;
			case STARTS_WITH:
				if (!cache.isCacheLengthGE(checkTerm.length(), counter)) {
					return null;
				}
				break;

			default:
				return null;
			}

			prefixLength += cache.getTotalLength(counter);
			byte[] contentToSearch = createContentToSearch(checkTerm, ck,
					sorted, operation, minLengthTerm, maxLengthTerm,
					prefixLength, counter);

			Set<String> tempSet = createSetToSearch(checkTerm.length(),
					contentToSearch, sorted, operation, minLengthTerm,
					maxLengthTerm, counter);
			ret.addAll(tempSet);
		}

		return ret;
	}

	private byte[] createContentToSearch(String term, CipherKey ck,
			SortedSet<Integer> sorted, int operation, int minLengthTerm,
			int maxLengthTerm, int prevIndexLen, int index) throws Exception {
		byte[] contentToSearch;
		String prefix = "";
		int prefixLen = 0;
		int length = term.getBytes().length;
		System.out.println("-- createContentToSearch: "+index+ ", prevIndexLen: "+prevIndexLen);
		for (int nextInt : sorted) {
//			System.out.println("next: "+nextInt);
			if (nextInt < minLengthTerm) {
				int count = cache.getCountByLength(nextInt, index);
				if(count == cache.NOT_FOUND) {
					continue;
				}
				prefixLen = count * (nextInt + 1);
//				System.out.println("prefixLen: "+prefixLen);
//				prefix += new String(new byte[prefixLen]);
				prefix += new String(new char[prefixLen]).replace("\0", "-");
			} else if (nextInt == minLengthTerm) {
				prefixLen = prefix.length();
				int fillerLength = minLengthTerm - length + 1;
				int size = cache.getCountByLength(nextInt, index);
				for (int i = 0; i < size; i++) {
					//prefix += term + new String(new byte[fillerLength]);
					prefix += term + new String(new char[fillerLength]).replace("\0", "-");
				}
				if (operation == NO_WC) {
					break;
				}
			} else if (nextInt > minLengthTerm && nextInt <= maxLengthTerm
					&& operation == STARTS_WITH) {
				// prefixLen = prefix.length();
				int size = cache.getCountByLength(nextInt, index);
				String termStr = term + new String(new byte[nextInt - length]);
				for (int i = 0; i < size; i++) {
					prefix += termStr + "-";
				}
			} else {
				break;
			}
		}

		byte[] toEncrypt = prefix.getBytes();
//		System.out.println("Current Cache size: "+toEncrypt.length);
//		System.out.println("Current Cache 1: "+ new String(toEncrypt));
		//byte[] toEncrypt2 = new byte[prefixLength + toEncrypt.length];
		byte[] toEncrypt2 = new String(new char[prevIndexLen + toEncrypt.length]).replace("\0", "-").getBytes();
		System.out.println("Total Cache size: "+toEncrypt2.length);
		for (int i = 0; i < toEncrypt.length; i++) {
			toEncrypt2[i + prevIndexLen] = toEncrypt[i];
		}
		//System.out.println("Current Cache: "+ new String(toEncrypt2));
		byte[] encContent = cryptoService.encryptBlock(toEncrypt2, ck);
//		System.out.println("encContent: "+cryptoService.bytesToHex(encContent));
		int contentLength = encContent.length - (prefixLen + prevIndexLen);
		contentToSearch = new byte[contentLength];

		for (int j = 0; j < contentToSearch.length; j++) { // Remove prefix from
															// search string
			contentToSearch[j] = encContent[prefixLen + prevIndexLen + j];

		}
//		System.out.println("contentToSearch: "+cryptoService.bytesToHex(contentToSearch));
		return contentToSearch;
	}

	private Set<String> createSetToSearch(int termLen, byte[] contentToSearch,
			SortedSet<Integer> sorted, int operation, int minLengthTerm,
			int maxLengthTerm, int counter) {
		Set<String> ret = new HashSet<String>();
		int accIndex = 0;
		for (Integer nextInt : sorted) {
			if (nextInt >= minLengthTerm) {

				accIndex = createBlock(termLen, contentToSearch, operation,
						ret, accIndex, nextInt, counter);
			}
			if (maxLengthTerm == nextInt) {
				break;
			}
		}
		return ret;
	}

	private int createBlock(int termLen, byte[] contentToSearch, int operation,
			Set<String> ret, int accIndex, Integer nextInt, int counter) {
		int chunkListLen = cache.getCountByLength(nextInt, counter);
		int beginIndex = 0;
		for (int i = 0; i < chunkListLen; i++) {
			beginIndex = (i * (nextInt + 1) + accIndex);
			byte[] byteTemp = new byte[termLen];
			for (int j = 0; j < byteTemp.length; j++) {
				byteTemp[j] = contentToSearch[beginIndex + j];
			}
			String searchHex = Converter.bytesToHex(byteTemp);
			if (operation == STARTS_WITH) {
				searchHex += "*";
			}
			ret.add(searchHex);
		}
		accIndex = beginIndex + nextInt + 1;
		return accIndex;
	}

}
