package cryptSearch.service.commons.crypto;

public interface KeyStore {

	public abstract byte[] getKeyBytes();

	public abstract byte[] getIvBytes();

	public abstract String getKeyType();

}