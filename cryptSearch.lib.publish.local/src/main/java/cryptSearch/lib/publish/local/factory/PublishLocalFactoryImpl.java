package cryptSearch.lib.publish.local.factory;

import cryptSearch.lib.publish.local.domain.ContentDiscovery;
import cryptSearch.lib.publish.local.domain.ContentDiscoveryFS;
import cryptSearch.lib.publish.local.message.ContentDiscoveryWork;
import cryptSearch.lib.publish.local.message.ContentDiscoveryWorkImpl;
import cryptSearch.lib.publish.local.util.ContentLoader;
import cryptSearch.lib.publish.local.util.ContentLoaderFileSystem;
import cryptSearch.lib.publish.local.util.PublishCryptoService;
import cryptSearch.lib.publish.local.util.PublishCryptoServiceAes;
import cryptSearch.lib.publish.local.util.Tokenizer;
import cryptSearch.lib.publish.local.util.TokenizerLuceneStandardAnalyzer;
import cryptSearch.service.commons.content.LocalBucketManager;
import cryptSearch.service.commons.content.LocalBucketManagerLocalFS;
import cryptSearch.service.commons.persist.CachePersist;
import cryptSearch.service.commons.persist.CachePersistJson;
import cryptSearch.service.commons.persist.CachePersistXStream;
import cryptSearch.service.commons.persist.RefByNamePersist;
import cryptSearch.service.commons.persist.RefByNamePersistJson;
import cryptSearch.service.commons.persist.RefByNamePersistXStream;
import cryptSearch.service.commons.persist.TokenListStore;
import cryptSearch.service.commons.persist.TokenListJson;
import cryptSearch.service.commons.util.CommonsFactory;
import cryptSearch.service.commons.util.CommonsFactoryImpl;


public class PublishLocalFactoryImpl implements PublishLocalFactory {
	private CommonsFactory commonsFactory = null;
	private static PublishLocalFactory factory;
	
	public static PublishLocalFactory getInstance(){
		if(factory== null){
			factory = new PublishLocalFactoryImpl();
		}
		return factory;
	}
	private PublishLocalFactoryImpl() {
		super();
		commonsFactory = CommonsFactoryImpl.getInstance();
	}

//	public CommonsFactory getCommonsFactory() {
//		return commonsFactory;
//	}
	
	public LocalBucketManager getLocalBucketManager(){
		return LocalBucketManagerLocalFS.getInstance();
		
	}
	
	public ContentLoader getContentLoader() {
		return new ContentLoaderFileSystem();
	}

	public Tokenizer getTokenizer() {
		return new TokenizerLuceneStandardAnalyzer();
	}

	public ContentDiscovery getContentDiscovery(String initialPath[], String[] filters)
	{
		return new ContentDiscoveryFS(initialPath, filters);
	}
	
	public RefByNamePersist getRefByNamePersist() 
	{
		return commonsFactory.getRefByNamePersist();
	}
	

	public CachePersist getCachePersist() 
	{
		return commonsFactory.getCachePersist();
	}
	
	@Override
	public TokenListStore getTokenList(String filePath) {
		TokenListStore tokenList = commonsFactory.getTokenList(filePath);
		tokenList.init(getLocalBucketManager());
		return tokenList;
	}
	
	
	public ContentDiscoveryWork getContentDiscoveryWork(String path, String[] filter) 
	{
		return new ContentDiscoveryWorkImpl(path,
				filter);
	}

	public PublishCryptoService getCryptoService() {
		return new PublishCryptoServiceAes(commonsFactory);
	}
	@Override
	public void initRemoteBucketManager(boolean delete) {
		commonsFactory.getRemoteBucketManager().init(delete);		
	}
	
	
}
