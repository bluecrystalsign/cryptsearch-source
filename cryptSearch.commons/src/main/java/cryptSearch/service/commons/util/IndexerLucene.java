package cryptSearch.service.commons.util;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.MMapDirectory;
import org.apache.lucene.util.Version;

import cryptSearch.service.commons.content.RemoteBucketManager;

public class IndexerLucene  {

	private Directory index;
	private StandardAnalyzer analyzer;
	private IndexWriter w;
	private Document doc;
	private TextField fieldContent;
	private StringField fieldUrl;
	private IndexSearcher searcher;
	private String indexName;

	private RemoteBucketManager bucketManager;
	public StandardAnalyzer getAnalyzer() {
		return analyzer;
	}

	public IndexerLucene(String indexName, RemoteBucketManager remoteBucketManager) {
		super();
		
		bucketManager = remoteBucketManager;
		try {
			
			File indexPath = bucketManager.clearIndexPath(indexName);
			index = MMapDirectory.open(indexPath.toPath());
			this.indexName = indexName;
		} catch (Exception e) {
			e.printStackTrace();
		}
		analyzer = new StandardAnalyzer();
		doc = new Document();
		fieldContent = new TextField("content", "",
				Field.Store.YES);
		doc.add(fieldContent);
		fieldUrl = new StringField("url", "", Field.Store.YES);
		doc.add(fieldUrl);
	}

}
