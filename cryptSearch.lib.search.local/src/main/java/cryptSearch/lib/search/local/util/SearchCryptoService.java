package cryptSearch.lib.search.local.util;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.NoSuchPaddingException;

import cryptSearch.service.commons.crypto.CipherKey;

public interface SearchCryptoService {

	CipherKey loadStreamKeyStore(String indexName) throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, Exception;

	byte[] encryptBlock(byte[] bytes, CipherKey ck) throws Exception;

	public byte[] decryptBlock(String ksname, byte[] content) throws Exception;

	boolean verifyIntegrity(byte[] sha256Orig, byte[] clearContent) throws NoSuchAlgorithmException;

	byte[] calcSha256(byte[] content) throws NoSuchAlgorithmException;

	String bytesToHex(byte[] bytes);
}