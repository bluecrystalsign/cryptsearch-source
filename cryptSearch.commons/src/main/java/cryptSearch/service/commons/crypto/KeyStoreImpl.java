package cryptSearch.service.commons.crypto;

public class KeyStoreImpl implements KeyStore {
	private byte[] keyBytes;
	private byte[] ivBytes;
	private String keyType;
	/* (non-Javadoc)
	 * @see cryptSearch.service.commons.crypto.KeyStore#getK()
	 */
	public byte[] getKeyBytes() {
		return keyBytes;
	}
	/* (non-Javadoc)
	 * @see cryptSearch.service.commons.crypto.KeyStore#getI()
	 */
	public byte[] getIvBytes() {
		return ivBytes;
	}
	
	/* (non-Javadoc)
	 * @see cryptSearch.service.commons.crypto.KeyStore#getT()
	 */
	public String getKeyType() {
		return keyType;
	}
	public KeyStoreImpl() {
		super();
	}
	public KeyStoreImpl(byte[] k, byte[] i, String t) {
		super();
		this.keyBytes = k;
		this.ivBytes = i;
		this.keyType = t;
	}



}
