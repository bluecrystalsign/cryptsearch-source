package cryptSearch.lib.search.local.factory;

import cryptSearch.lib.search.local.util.RegexAnalysis;
import cryptSearch.lib.search.local.util.RegexAnalysisLucene;
import cryptSearch.lib.search.local.util.SearchCryptoService;
import cryptSearch.lib.search.local.util.SearchCryptoServiceAes;
import cryptSearch.service.commons.content.LocalBucketManager;
import cryptSearch.service.commons.content.LocalBucketManagerLocalFS;
import cryptSearch.service.commons.persist.CachePersist;
import cryptSearch.service.commons.persist.CachePersistJson;
import cryptSearch.service.commons.persist.CachePersistXStream;
import cryptSearch.service.commons.persist.RefByNamePersist;
import cryptSearch.service.commons.persist.RefByNamePersistJson;
import cryptSearch.service.commons.util.CacheHandler;
import cryptSearch.service.commons.util.CacheHandlerImpl;
import cryptSearch.service.commons.util.CommonsFactory;
import cryptSearch.service.commons.util.CommonsFactoryImpl;


public class SearchLocalFactoryImpl implements SearchLocalFactory {
	private CommonsFactory cFactory;
	private static SearchLocalFactory factory;
	private RegexAnalysis regexAnalysis; 
	
	@Override
	public RegexAnalysis getRegexAnalysis() {
		if(regexAnalysis == null) {
			regexAnalysis = new RegexAnalysisLucene(true);
		}
		return regexAnalysis;
	}

	public static SearchLocalFactory getInstance(){
		if(factory== null){
			factory = new SearchLocalFactoryImpl();
		}
		return factory;
	}

	private SearchLocalFactoryImpl() {
		super();
		cFactory = CommonsFactoryImpl.getInstance();
	}

	public LocalBucketManager getLocalBucketManager() {
		
		return cFactory.getLocalBucketManager();
	}
	public CachePersist getCachePersist() 
	{
		return cFactory.getCachePersist();
	}

	public SearchCryptoService getCryptoService() {
		return new SearchCryptoServiceAes(cFactory);
	}

	public CacheHandler getCacheHandler(boolean delete) {
		return new CacheHandlerImpl(getCachePersist(), getLocalBucketManager(), delete);
	}
	
	@Override
	public RefByNamePersist getRefByNamePersist() 
	{
		return cFactory.getRefByNamePersist();
	}
}
