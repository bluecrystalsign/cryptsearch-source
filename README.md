# Crypt Search

1: Install virtual box (https://www.virtualbox.org/wiki/Downloads)
2: Install Vagrant (https://www.vagrantup.com/downloads.html)
3: Download & extract 'dist.zip'
4: Open a Windows CMD and change dir to folder in (3:)
5: exec 'vagrant up'
6: exec 'vagrant ssh'

## You are in a Ubuntu 18 shell and should type the following commands

cd /vagrant/
cd dist/
./step1.sh myindex ../samples/pack1
./step2.sh myindex
./step3.sh surface myindex

Step 1 - cria as estruturas criptografadas
Step 2 - monta o indice Lucene
Step 3 - Faz a busca do termo 'surface' no indice e retorna 2 pdfs.