package cryptSearch.service.commons.persist;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

import cryptSearch.service.commons.content.LocalBucketManager;
import cryptSearch.service.commons.content.RemoteBucketManager;

public interface TokenListStore {

	void persistLocal(String indexName, Map<String, Set<String>> tokensToIndex) throws FileNotFoundException, IOException;

	Map<String, Object> loadRemote(String indexName) throws FileNotFoundException, IOException;

	void init(LocalBucketManager bucket);

	void init(RemoteBucketManager bucket);

	void deleteRemote(String indexName) throws FileNotFoundException, IOException;

	Map<String, Object> loadLocal(String indexName) throws FileNotFoundException, IOException;

}