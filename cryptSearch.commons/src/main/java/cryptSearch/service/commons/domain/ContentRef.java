package cryptSearch.service.commons.domain;

import java.util.Arrays;

public class ContentRef {
	private String name;
	private byte[] sha256;
	public ContentRef() {
		super();
	}
	public ContentRef(String name, byte[] sha256) {
		super();
		this.name = name;
		this.sha256 = sha256;
	}
	public String getName() {
		return name;
	}
	public byte[] getSha256() {
		return sha256;
	}
	@Override
	public String toString() {
//		return "ContentRef [name=" + name + ", sha256=" + Arrays.toString(sha256) + "]";
		return "ContentRef [name=" + name + ", sha256=.]";
	}
	

}
