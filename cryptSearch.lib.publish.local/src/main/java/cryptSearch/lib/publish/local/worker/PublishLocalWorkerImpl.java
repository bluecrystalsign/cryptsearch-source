package cryptSearch.lib.publish.local.worker;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cryptSearch.lib.publish.local.domain.ContentDiscovery;
import cryptSearch.lib.publish.local.domain.ContentDiscoveryFS;
import cryptSearch.lib.publish.local.factory.PublishLocalFactory;
import cryptSearch.lib.publish.local.message.ContentUri;
import cryptSearch.lib.publish.local.message.Tokens;
import cryptSearch.lib.publish.local.util.PublishCryptoService;
import cryptSearch.service.commons.crypto.CipherKey;
import cryptSearch.service.commons.domain.ContentRef;
import cryptSearch.service.commons.persist.CachePersist;
import cryptSearch.service.commons.persist.RefByNamePersist;
import cryptSearch.service.commons.persist.TokenListStore;
import cryptSearch.service.commons.util.CacheHandler;
import cryptSearch.service.commons.util.Converter;

public class PublishLocalWorkerImpl implements PublishLocalWorker {

	private PublishLocalFactory plf;
	// private CommonsFactory cf;
	private String[] initialPath;
	private String[] filters;
	private CacheHandler cacheHandler;
	private boolean doHash;

	public PublishLocalWorkerImpl(PublishLocalFactory plf,
			String[] initialPath, String[] filters, CacheHandler cacheHandler, boolean doHash) {
		super();
		this.plf = plf;
		this.initialPath = initialPath;
		this.filters = filters;
		this.cacheHandler = cacheHandler;
		this.doHash = doHash;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cryptSearch.lib.publish.local.worker.PublishLocalWorker#createEncryptedTokens
	 * (java.lang.String[], java.lang.String[], java.lang.String)
	 */
	public void createEncryptedTokens(String indexName)
			throws Exception {

		Map<Integer, Set<String>> tokensByLength = new HashMap<Integer, Set<String>>();
		Map<String, Tokens> tokenByDocumnent = new HashMap<String, Tokens>();
		Map<Integer, ContentRef> contentReferenceByName = new HashMap<Integer, ContentRef>();

		resetOuptupBucket(true);

		List<ContentUri> listOfListOfUris = discover(plf.getContentDiscovery(initialPath,
				filters));
		
		System.out.println(Messages.getString("PublishLocalWorkerImpl.0")+listOfListOfUris.get(0).getFileList().size()); //$NON-NLS-1$
		System.out.println(Messages.getString("PublishLocalWorkerImpl.1")+listOfListOfUris); //$NON-NLS-1$
		
		tokenizeContent(indexName, listOfListOfUris, tokensByLength, tokenByDocumnent, contentReferenceByName);
		
		CipherKey indexKey = plf.getCryptoService().createCihperKeyStream(indexName);
		Map<String, String> encMap = plf.getCryptoService().encryptIndex(tokensByLength, indexKey, 0);
		// 0 -> the index will be created, so there's no prefix.

		persistTokensBySize(indexName, tokensByLength, true);
		persistContentReference(indexName, contentReferenceByName, true);
		Map<String, Set<String>> encryptedTokenByDocumnent = buildIndex(encMap,	tokenByDocumnent);
		persistTokenListStore(indexName, encryptedTokenByDocumnent);
	}

	private void persistTokenListStore(String indexName, Map<String, Set<String>> tokensToIndex)
			throws FileNotFoundException, IOException {
		TokenListStore tokenListStore = plf.getTokenList(""); //$NON-NLS-1$
		tokenListStore.persistLocal(indexName, tokensToIndex);
	}

	private Map<String, Object> loadTokenListStore(String indexName)
			throws FileNotFoundException, IOException {
		TokenListStore tokenListStore = plf.getTokenList(""); //$NON-NLS-1$
		return tokenListStore.loadLocal(indexName);
	}

	private void tokenizeContent(String indexName, List<ContentUri> listOfListOfUris,
			Map<Integer, Set<String>> tokensByLength, Map<String, Tokens> tokenByDocumnent,
			Map<Integer, ContentRef> contentReferenceByName) throws Exception {
		int i = 0;
		String mtsStr = Messages.getString("PublishLocalWorkerImpl.7"); //$NON-NLS-1$
		Integer minTokenSize = Integer.parseInt(mtsStr);
		for (ContentUri nextCdr : listOfListOfUris) {
			for (String nextPathToProcess : nextCdr.getFileList()) {

				String keyName = indexName + Messages.getString("PublishLocalWorkerImpl.2") + i; //$NON-NLS-1$
				CipherKey contentKey = plf.getCryptoService().createCihperKeyBlock(keyName); //$NON-NLS-1$
				
				byte[] contentHash = plf.getCryptoService().loadEncryptPersistContent(nextPathToProcess,	contentKey, i);
				contentReferenceByName.put(i, new ContentRef(nextPathToProcess, contentHash));
				
				String parsedContentAsText = plf.getContentLoader().parseContentToString(nextPathToProcess);
				Set<String> tokenizedParsedContent = plf.getTokenizer().tokenizeString(parsedContentAsText);
				

				Iterator<String> it = tokenizedParsedContent.iterator();
				while(it.hasNext()) {
					String next = it.next();
					if(next.length() < minTokenSize) {
						it.remove();
					}
				}
				
				if(doHash) {
					tokenizedParsedContent = getHashedTokens(tokenizedParsedContent);			
				}

				for (String nextToken : tokenizedParsedContent) {
					agrregateTokenByLength(nextToken, tokensByLength);
				}
				Tokens tr = new Tokens(tokenizedParsedContent, i);
				tokenByDocumnent.put(nextPathToProcess, tr);
				i++;
			}
		}
		System.out.println("Counting tokens by length (min length: "+minTokenSize+")"); //$NON-NLS-1$
		long totalSize = 0l;
		Set<String> AllOfIt = new HashSet<>();
		for(Integer next : tokensByLength.keySet()) {
			System.out.println(next + "->"+tokensByLength.get(next).size()); //$NON-NLS-1$
			totalSize += tokensByLength.get(next).size();
			AllOfIt.addAll(tokensByLength.get(next));
		}
		
		System.out.println("Total token count ->"+totalSize); //$NON-NLS-1$
//		System.out.println(AllOfIt); //$NON-NLS-1$
		//debugTokensToFile(AllOfIt);
	}

	private void debugTokensToFile(Set<String> allOfIt) {
		try {
			String str = allOfIt.toString();
			BufferedWriter writer = new BufferedWriter(new FileWriter("c:\\lixo\\tokens.txt"));
			writer.write(str);
			
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private Set<String> getHashedTokens(Set<String> origTokens) {
		Set<String> hashedTokens = new HashSet<String>();
		Iterator<String> it = origTokens.iterator();
		try {
			while(it.hasNext()) {
				String next = it.next();
				byte[] hashedBytes = plf.getCryptoService().calcSha256(next.getBytes());
				String searchHex = Converter.bytesToHex(hashedBytes);
				hashedTokens.add(searchHex);
			}
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return hashedTokens;
	}

	public Map<String, Set<String>> createEncryptedTokensToUpdate(
			String indexName) throws Exception {
		Map<Integer, Set<String>> tokensByLength = new HashMap<Integer, Set<String>>();
		Map<String, Tokens> tokenByDocumnent = new HashMap<String, Tokens>();
		Map<Integer, ContentRef> refByName = new HashMap<Integer, ContentRef>();

		resetOuptupBucket(false);
		initCacheHandler(indexName);

		List<ContentUri> cdr = discover(plf.getContentDiscovery(initialPath,
				filters));
		PublishCryptoService cryptoService = plf.getCryptoService();

		int nextContentIndex = cryptoService.getNextContentIndex();
		for (ContentUri nextCdr : cdr) {
			for (String nextPath : nextCdr.getFileList()) {

				String parsed = plf.getContentLoader().parseContentToString(
						nextPath);
				CipherKey cbcCipherKey = cryptoService
						.createCihperKeyBlock(indexName + Messages.getString("PublishLocalWorkerImpl.3") //$NON-NLS-1$
								+ nextContentIndex);
				byte[] sha256 = cryptoService.loadEncryptPersistContent(nextPath,
						cbcCipherKey, nextContentIndex);
				refByName.put(nextContentIndex,
						new ContentRef(nextPath, sha256));
				Set<String> tokenizeString = plf.getTokenizer().tokenizeString(
						parsed);
				for (String nextToken : tokenizeString) {
					agrregateTokenByLength(nextToken, tokensByLength);
				}
				Tokens tr = new Tokens(tokenizeString, nextContentIndex);
				tokenByDocumnent.put(nextPath, tr);
				nextContentIndex++;
			}
		}

		// end content loading and tokenization
		
		// Create encrypted index

		int maxCache = cacheHandler.getCount();
		Map<String, Set<String>> tokensToIndex =  new HashMap<String, Set<String>>();
//		for (int i = 0; i < maxCache; i++) {
			CipherKey ctrCipherKey = cryptoService
					.loadStreamKeyStore(indexName);
			int prefixLength = cacheHandler.getTotalLength(maxCache);

			Map<String, String> encMap = cryptoService.encryptIndex(
					tokensByLength, ctrCipherKey, prefixLength);

			int i = 0;
			for (String next : encMap.keySet()) {
				System.out
						.println(Messages.getString("PublishLocalWorkerImpl.4") + next + Messages.getString("PublishLocalWorkerImpl.5") + encMap.get(next) + Messages.getString("PublishLocalWorkerImpl.6")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				i++;
				if(i == 5) break;
			}
			persistTokensBySize(indexName, tokensByLength, false);
			System.out.println("refByName");
			for(Integer next : refByName.keySet()) {
				System.out.println(next + "->" + refByName.get(next));
			}
			persistContentReference(indexName, refByName, false);
			Map<String, Set<String>> tokens = buildIndex(encMap,
					tokenByDocumnent);
			System.out.println("tokens Key count: " + tokens.keySet().size());
			for(String next : tokens.keySet()) {
				Set<String> value = tokens.get(next);
				System.out.println("tokens Value count: " + next + "->" + value.size());
			}
			tokensToIndex.putAll(tokens);
//		}
			
//		Map<String, Object> loadedTokens = loadTokenListStore(indexName);
//		for(String next : loadedTokens.keySet()) {
//			Object object = loadedTokens.get(next);
//			List<String> list = (List<String>) object;
//			Set<String> targetSet = new HashSet<>(list);
//			tokensToIndex.put(next, targetSet);
//		}
		
		persistTokenListStore(indexName, tokensToIndex);
			
		return tokensToIndex;
	}

	// private int countPrefix() {
	// int prefixLen = 0;
	// SortedSet<Integer> sorted = cacheHandler.getSorted();
	//
	// for (int nextInt : sorted) {
	// int count = cacheHandler.getCacheLength(nextInt);
	// prefixLen += count * (nextInt + 1);
	// }
	// return prefixLen;
	// }

	private void initCacheHandler(String indexName) throws Exception {
		cacheHandler.loadCache(indexName);

	}

	private void persistContentReference(String indexName,
			Map<Integer, ContentRef> refByName, boolean overwrite)
			throws Exception {
		RefByNamePersist pers = plf.getRefByNamePersist();
		pers.persist(refByName, indexName, overwrite);

	}

	private void persistTokensBySize(String indexName,
			Map<Integer, Set<String>> sumTokens, boolean delete)
			throws Exception {
		CachePersist pers = plf.getCachePersist();
		pers.initLocalBucketManager(delete);
		pers.persistCache(indexName, sumTokens);
	}

	private List<ContentUri> discover(ContentDiscovery cd) {
		List<ContentUri> ret = null;
		if (cd instanceof ContentDiscoveryFS) {
			// ret = discoverFiles((ContentDiscoveryFS) cd);
			ret = cd.discoverFiles(plf);
		}
		return ret;
	}

	private void resetOuptupBucket(boolean delete) {
		plf.initRemoteBucketManager(delete);

	}

	private void agrregateTokenByLength(String nextStr,
			Map<Integer, Set<String>> tokensByLength) {
		if (!tokensByLength.containsKey(nextStr.length())) {
			tokensByLength.put(nextStr.length(), new HashSet<String>());
		}
		tokensByLength.get(nextStr.length()).add(nextStr);

	}

	private Map<String, Set<String>> buildIndex(Map<String, String> encMap,
			Map<String, Tokens> tokenByDocumnent) throws Exception {
		Map<String, Set<String>> tokensToIndex = new HashMap<String, Set<String>>();

		for (String next : tokenByDocumnent.keySet()) {
			Tokens tokens = tokenByDocumnent.get(next);
			Set<String> encTokens = tokens.getEncTokens();
			for (String nextToEnc : tokens.getTokens()) {
				String encTerm = encMap.get(nextToEnc);
				encTokens.add(encTerm);
			}

			String valueOf = String.valueOf(tokens.getI());
			tokensToIndex.put(valueOf, encTokens);
		}
		return tokensToIndex;
	}

}
