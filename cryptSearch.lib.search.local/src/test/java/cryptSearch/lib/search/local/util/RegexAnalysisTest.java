package cryptSearch.lib.search.local.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.RegexpQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.junit.jupiter.api.Test;

class RegexAnalysisTest {
	private RegexAnalysis regexAnalysis= new RegexAnalysisLucene(false); 

	@Test
	void testAnaliseRegex01() {
		try {
			System.out.println("-- started");
			
			String original = "crypto.*";
			System.out.println(original);
			
			long hits = search(original);
			System.out.println("Hits -->" + hits);
			assertEquals(1l, hits);

			RegexTips ret = regexAnalysis.analiseRegex(original);
			
			RegexAnalysis.Usage[] usage = {null, null, null, null, null, null, 
					RegexAnalysis.Usage.ANY_CHAR_ONCE, 
					RegexAnalysis.Usage.PRECEDING_ZERO_PLUS}; 
			RegexTips check = 
					new RegexTips(usage, 
							6, RegexAnalysis.UNLIMITED);
			
			System.out.println(ret);
			System.out.println(check);
			if(!ret.equals(check)) {
				fail("not returned the expected");
			}
			
			System.out.println("-- end");
			
		}catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
	}

	@Test
	void testAnaliseRegex02() {
		try {
			System.out.println("-- started");
			
			String original = "crypto.+";
			System.out.println(original);
			
			long hits = search(original);
			System.out.println("Hits -->" + hits);
			assertEquals(1l, hits);

			RegexTips ret = regexAnalysis.analiseRegex(original);
			
			RegexAnalysis.Usage[] usage = {null, null, null, null, null, null, 
					RegexAnalysis.Usage.ANY_CHAR_ONCE, 
					RegexAnalysis.Usage.PRECEDING_ONE_PLUS}; 
			RegexTips check = 
					new RegexTips(usage, 
							7, RegexAnalysis.UNLIMITED);
			
			System.out.println(ret);
			System.out.println(check);
			if(!ret.equals(check)) {
				fail("not returned the expected");
			}
			System.out.println("-- end");
			
		}catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
	}

	@Test
	void testAnaliseRegex03() {
		try {
			System.out.println("-- started");
			
			String original = "crypto.*phy";
			System.out.println(original);
			
			long hits = search(original);
			System.out.println("Hits -->" + hits);
			assertEquals(1l, hits);

			RegexTips ret = regexAnalysis.analiseRegex(original);
			
			RegexAnalysis.Usage[] usage = {null, null, null, null, null, null, 
					RegexAnalysis.Usage.ANY_CHAR_ONCE, 
					RegexAnalysis.Usage.PRECEDING_ZERO_PLUS,
					null, null, null}; 
			RegexTips check = 
					new RegexTips(usage, 
							9, RegexAnalysis.UNLIMITED);
			
			System.out.println(ret);
			System.out.println(check);
			if(!ret.equals(check)) {
				fail("not returned the expected");
			}
			System.out.println("-- end");
			
		}catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
	}

	@Test
	void testAnaliseRegex04() {
		try {
			System.out.println("-- started");
			
			String original = "cryptograph.";
			System.out.println(original);
			
			long hits = search(original);
			System.out.println("Hits -->" + hits);
			assertEquals(1l, hits);

			RegexTips ret = regexAnalysis.analiseRegex(original);
			
			RegexAnalysis.Usage[] usage = {null, null, null, null, null, null, null, null, null, null, null,  
					RegexAnalysis.Usage.ANY_CHAR_ONCE}; 
			RegexTips check = 
					new RegexTips(usage, 
							12, 12);
			
			System.out.println(ret);
			System.out.println(check);
			if(!ret.equals(check)) {
				fail("not returned the expected");
			}
			System.out.println("-- end");
			
		}catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
	}

	@Test
	void testAnaliseRegex05() {
		try {
			System.out.println("-- started");
			
			String original = "cryptograp..";
			System.out.println(original);
			
			long hits = search(original);
			System.out.println("Hits -->" + hits);
			assertEquals(1l, hits);

			RegexTips ret = regexAnalysis.analiseRegex(original);
			
			RegexAnalysis.Usage[] usage = {null, null, null, null, null, null, null, null, null, null, 
					RegexAnalysis.Usage.ANY_CHAR_ONCE, 
					RegexAnalysis.Usage.ANY_CHAR_ONCE}; 
			RegexTips check = 
					new RegexTips(usage, 
							12, 12);
			
			System.out.println(ret);
			System.out.println(check);
			if(!ret.equals(check)) {
				fail("not returned the expected");
			}
			System.out.println("-- end");
			
		}catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
	}

	@Test
	void testAnaliseRegex06() {
		try {
			System.out.println("-- started");
			
			String original = "cryptograp.{2}";
			System.out.println(original);
			
			long hits = search(original);
			System.out.println("Hits -->" + hits);
			assertEquals(1l, hits);

			RegexTips ret = regexAnalysis.analiseRegex(original);
			
			RegexAnalysis.Usage[] usage = {null, null, null, null, null, null, null, null, null, null, 
					RegexAnalysis.Usage.ANY_CHAR_ONCE, 
					RegexAnalysis.Usage.START_REPEAT_COUNT,
					null,
					RegexAnalysis.Usage.END_REPEAT_COUNT
					}; 
			RegexTips check = 
					new RegexTips(usage, 
							12, 12);
			
			System.out.println(ret);
			System.out.println(check);
			if(!ret.equals(check)) {
				fail("not returned the expected");
			}
			System.out.println("-- end");
			
		}catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
	}

	@Test
	void testAnaliseRegex07() {
		try {
			System.out.println("-- started");
			
			String original = "crypto..aphy";
			System.out.println(original);
			
			long hits = search(original);
			System.out.println("Hits -->" + hits);
			assertEquals(1l, hits);

			RegexTips ret = regexAnalysis.analiseRegex(original);
			
			RegexAnalysis.Usage[] usage = {null, null, null, null, null, null, 
					RegexAnalysis.Usage.ANY_CHAR_ONCE, RegexAnalysis.Usage.ANY_CHAR_ONCE, 
					null, null, null, null}; 
			RegexTips check = 
					new RegexTips(usage, 
							12, 12);
			
			System.out.println(ret);
			System.out.println(check);
			if(!ret.equals(check)) {
				fail("not returned the expected");
			}
			System.out.println("-- end");
			
		}catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
	}

	@Test
	void testAnaliseRegex08() {
		try {
			System.out.println("-- started");
			
			String original = "crypto.?.?aphy";
			System.out.println(original);
			
			long hits = search(original);
			System.out.println("Hits -->" + hits);
			assertEquals(1l, hits);

			RegexTips ret = regexAnalysis.analiseRegex(original);
			
			RegexAnalysis.Usage[] usage = {null, null, null, null, null, null, 
					RegexAnalysis.Usage.ANY_CHAR_ONCE, 
					RegexAnalysis.Usage.PRECEDING_OPTIONAL, 
					RegexAnalysis.Usage.ANY_CHAR_ONCE, 
					RegexAnalysis.Usage.PRECEDING_OPTIONAL, null, null, null, null}; 
			RegexTips check = 
					new RegexTips(usage, 
							10, 12);
			
			System.out.println(ret);
			System.out.println(check);
			if(!ret.equals(check)) {
				fail("not returned the expected");
			}
			System.out.println("-- end");
			
		}catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
	}

	@Test
	void testAnaliseRegex09() {
		try {
			System.out.println("-- started");
			
			String original = "crypto.{2}aphy";
			System.out.println(original);
			
			long hits = search(original);
			System.out.println("Hits -->" + hits);
			assertEquals(1l, hits);

			RegexTips ret = regexAnalysis.analiseRegex(original);
			
			RegexAnalysis.Usage[] usage = {null, null, null, null, null, null, 
					RegexAnalysis.Usage.ANY_CHAR_ONCE, RegexAnalysis.Usage.START_REPEAT_COUNT, null, 
					RegexAnalysis.Usage.END_REPEAT_COUNT, null, null, null, null}; 
			RegexTips check = 
					new RegexTips(usage, 
							12, 12);
			
			System.out.println(ret);
			System.out.println(check);
			if(!ret.equals(check)) {
				fail("not returned the expected");
			}
			System.out.println("-- end");
			
		}catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
	}

	@Test
	void testAnaliseRegex10() {
		try {
			System.out.println("-- started");
			
			String original = "crypto...?phy";
			System.out.println(original);
			
			long hits = search(original);
			System.out.println("Hits -->" + hits);
			assertEquals(1l, hits);

			RegexTips ret = regexAnalysis.analiseRegex(original);
			
			RegexAnalysis.Usage[] usage = {null, null, null, null, null, null, 
					RegexAnalysis.Usage.ANY_CHAR_ONCE, 
					RegexAnalysis.Usage.ANY_CHAR_ONCE, 
					RegexAnalysis.Usage.ANY_CHAR_ONCE, 
					RegexAnalysis.Usage.PRECEDING_OPTIONAL, null, null, null}; 
			RegexTips check = 
					new RegexTips(usage, 
							11, 12);
			
			System.out.println(ret);
			System.out.println(check);
			if(!ret.equals(check)) {
				fail("not returned the expected");
			}
			System.out.println("-- end");
			
		}catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
	}

	@Test
	void testAnaliseRegex11() {
		try {
			System.out.println("-- started");
			
			String original = "crypto...+phy";
			System.out.println(original);
			
			long hits = search(original);
			System.out.println("Hits -->" + hits);
			assertEquals(1l, hits);

			RegexTips ret = regexAnalysis.analiseRegex(original);
			
			RegexAnalysis.Usage[] usage = {null, null, null, null, null, null, 
					RegexAnalysis.Usage.ANY_CHAR_ONCE, RegexAnalysis.Usage.ANY_CHAR_ONCE, 
					RegexAnalysis.Usage.ANY_CHAR_ONCE, RegexAnalysis.Usage.PRECEDING_ONE_PLUS, null, null, null}; 
			RegexTips check = 
					new RegexTips(usage, 
							12, RegexAnalysis.UNLIMITED);
			
			System.out.println(ret);
			System.out.println(check);
			if(!ret.equals(check)) {
				fail("not returned the expected");
			}
			System.out.println("-- end");
			
		}catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
	}

	@Test
	void testAnaliseRegex12() {
		try {
			System.out.println("-- started");
			
			String original = "crypto....?phy";
			System.out.println(original);
			
			long hits = search(original);
			System.out.println("Hits -->" + hits);
			assertEquals(1l, hits);

			RegexTips ret = regexAnalysis.analiseRegex(original);
			
			RegexAnalysis.Usage[] usage = {null, null, null, null, null, null, 
					RegexAnalysis.Usage.ANY_CHAR_ONCE, RegexAnalysis.Usage.ANY_CHAR_ONCE, 
					RegexAnalysis.Usage.ANY_CHAR_ONCE, RegexAnalysis.Usage.ANY_CHAR_ONCE, 
					RegexAnalysis.Usage.PRECEDING_OPTIONAL, null, null, null}; 

			RegexTips check = 
					new RegexTips(usage, 
							11, 12);
			
			System.out.println(ret);
			System.out.println(check);
			if(!ret.equals(check)) {
				fail("not returned the expected");
			}
			System.out.println("-- end");
			
		}catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
	}

	@Test
	void testAnaliseRegex13() {
		try {
			System.out.println("-- started");
			
			String original = "crypto...phy";
			System.out.println(original);
			
			long hits = search(original);
			System.out.println("Hits -->" + hits);
			assertEquals(1l, hits);

			RegexTips ret = regexAnalysis.analiseRegex(original);
			
			RegexAnalysis.Usage[] usage = {null, null, null, null, null, null, 
					RegexAnalysis.Usage.ANY_CHAR_ONCE, RegexAnalysis.Usage.ANY_CHAR_ONCE, 
					RegexAnalysis.Usage.ANY_CHAR_ONCE, null, null, null}; 

			RegexTips check = 
					new RegexTips(usage, 
							12, 12);
			
			System.out.println(ret);
			System.out.println(check);
			if(!ret.equals(check)) {
				fail("not returned the expected");
			}
			System.out.println("-- end");
			
		}catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
	}

	@Test
	void testAnaliseRegex14() {
		try {
			System.out.println("-- started");
			
			// . escaped
			String original = "crypto.\\..phy";
			System.out.println(original);
			RegexTips ret = regexAnalysis.analiseRegex(original);
			
			RegexAnalysis.Usage[] usage = {null, null, null, null, null, null, 
					RegexAnalysis.Usage.ANY_CHAR_ONCE, RegexAnalysis.Usage.ESCAPE, 
					RegexAnalysis.Usage.ANY_CHAR_ONCE, RegexAnalysis.Usage.ANY_CHAR_ONCE, null, null, null}; 

			RegexTips check = 
					new RegexTips(usage, 
							12, 12);
			
			System.out.println(ret);
			System.out.println(check);
			if(!ret.equals(check)) {
				fail("not returned the expected");
			}
			System.out.println("-- end");
			
		}catch (Exception e) {
			fail(e.getLocalizedMessage());
		}
	}

//	Hits for Hello -->2
//	Hits for hello ->2
//	Hits for Hi there -->0
	
	@Test
	void luceneHelloWorld() {
		try {
			//New index
			 StandardAnalyzer standardAnalyzer = new StandardAnalyzer();
			 Directory directory = new RAMDirectory();
			 IndexWriterConfig config = new IndexWriterConfig(standardAnalyzer); 
			 //Create a writer
			 IndexWriter writer = new IndexWriter(directory, config);
			 Document document = new Document ();
			 //In a real world example, content would be the actual content that needs to be indexed.
			 //Setting content to Hello World as an example.
			 document.add(new TextField("content", "Hello World", Field.Store.YES));
			 writer.addDocument(document);
			 document.add(new TextField("content", "Hello people", Field.Store.YES));
			 writer.addDocument(document); 
			 writer.close();
			  
			 //Now let's try to search for Hello
			 IndexReader reader = DirectoryReader.open(directory);
			 IndexSearcher searcher = new IndexSearcher (reader);
			 QueryParser parser = new QueryParser ("content", standardAnalyzer);
			 
			 Query query = parser.parse("Hello");
			 TopDocs results = searcher.search(query, 5);
			 System.out.println("Hits for Hello -->" + results.totalHits);
			 
			 //case insensitive search
			 query = parser.parse("hello");
			 results = searcher.search(query, 5);
			 System.out.println("Hits for hello -->" + results.totalHits);
			 
			 //search for a value not indexed
			 query = parser.parse("Hi there");
			 results = searcher.search(query, 5);
			 System.out.println("Hits for Hi there -->" + results.totalHits);
		} catch (IOException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
//	Hits for Hello -->2
//	Hits for hello ->2
//	Hits for Hi there -->0
	
	@Test
	void search() {
		long ret = search("Crypto*");
		System.out.println("Hits -->" + ret);
		assertEquals(1l, ret);
	}
	
	long search(String term) {
		try {
			//New index
			 StandardAnalyzer standardAnalyzer = new StandardAnalyzer();
			 Directory directory = new RAMDirectory();
			 IndexWriterConfig config = new IndexWriterConfig(standardAnalyzer); 
			 //Create a writer
			 IndexWriter writer = new IndexWriter(directory, config);
			 Document document = new Document ();
			 //In a real world example, content would be the actual content that needs to be indexed.
			 //Setting content to Hello World as an example.
			 document.add(new TextField("content", "cryptography", Field.Store.YES));
			 writer.addDocument(document); 
			 writer.close();
			  
			 //Now let's try to search for Hello
			 IndexReader reader = DirectoryReader.open(directory);
			 IndexSearcher searcher = new IndexSearcher (reader);
			 QueryParser parser = new QueryParser ("content", standardAnalyzer);
			 
			 Query query = new RegexpQuery(new Term("content", term));
			 query = query.rewrite(reader);
			 
//			 Query query = parser.parse(term);
			 TopDocs results = searcher.search(query, 5);
			 return results.totalHits.value;
			 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1l;
	}

}
