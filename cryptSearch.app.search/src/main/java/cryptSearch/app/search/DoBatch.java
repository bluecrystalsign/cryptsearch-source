package cryptSearch.app.search;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cryptSearch.lib.search.local.factory.SearchLocalFactory;
import cryptSearch.lib.search.local.factory.SearchLocalFactoryImpl;
import cryptSearch.lib.search.local.util.SearchCryptoService;
import cryptSearch.lib.search.local.worker.SearchLocalWorker;
import cryptSearch.lib.search.local.worker.SearchLocalWorkerImpl;
import cryptSearch.lib.search.remote.factory.SearchRemoteFactory;
import cryptSearch.lib.search.remote.factory.SearchRemoteFactoryImpl;
import cryptSearch.lib.search.remote.worker.SearchRemoteWorker;
import cryptSearch.lib.search.remote.worker.SearchRemoteWorkerImpl;
import cryptSearch.service.commons.content.LocalBucketManager;
import cryptSearch.service.commons.content.RemoteBucketManager;
import cryptSearch.service.commons.domain.ContentRef;
import cryptSearch.service.commons.persist.RefByNamePersist;
import cryptSearch.service.commons.util.CommonsFactory;
import cryptSearch.service.commons.util.CommonsFactoryImpl;
import cryptSearch.service.commons.util.Converter;
import cryptSearch.service.commons.util.Timelapse;

public class DoBatch {

	private static final String PARM_HASH = "--hashed";

	private SearchLocalFactory searchLocalFactory;
	private SearchRemoteFactory searchRemoteFactory;
	private CommonsFactory cf;

	public DoBatch() {
		super();
		searchLocalFactory = SearchLocalFactoryImpl.getInstance();
		cf = CommonsFactoryImpl.getInstance();
		searchRemoteFactory = SearchRemoteFactoryImpl.getInstance();
	}

	public static void main(String[] args) {
		(new DoBatch()).start(args);

	}

	private void start(String[] args) {
		String termsFile = args[args.length - 2];
		String indexName = args[args.length - 1];

		List<String> behaviour = null;
		if (args.length > 2) {
			behaviour = Arrays.asList(Arrays.copyOfRange(args, 0, args.length - 2));
		}
		boolean doHash = (behaviour != null && behaviour.contains(PARM_HASH));

		SearchRemoteWorker rs = new SearchRemoteWorkerImpl(searchRemoteFactory, indexName);

		try {
			// --dump --d
			if (behaviour != null && (behaviour.contains(Messages.getString("DoSearch.0"))
					|| behaviour.contains(Messages.getString("DoSearch.1")))) { // $NON-NLS-1$ //$NON-NLS-2$
				String termsCount = args[args.length - 2];
				System.out.println(Messages.getString("DoSearch.2") + indexName //$NON-NLS-1$
						+ Messages.getString("DoSearch.3") + termsCount); //$NON-NLS-1$

				Map<String, List<String>> hitList = rs.indexDump(indexName, termsCount);
				for (String next : hitList.keySet()) {
					System.out.println(Messages.getString("DoSearch.4") + next); //$NON-NLS-1$
					List<String> hit = hitList.get(next);
					for (String next2 : hit) {
						System.out.print(next2 + Messages.getString("DoSearch.5")); //$NON-NLS-1$
					}
				}
				System.out.println(Messages.getString("DoSearch.6")); //$NON-NLS-1$

			} else {

				List<String> termsList = listOfTerms(termsFile);
				System.out.println(termsList);
				for (String nextTerm : termsList) {
					execSearch(nextTerm, indexName, rs, doHash);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private List<String> listOfTerms(String termsFile) throws Exception {
		InputStream is = new FileInputStream(termsFile);
		BufferedReader buf = new BufferedReader(new InputStreamReader(is));

		String line = buf.readLine();
		StringBuilder sb = new StringBuilder();

		while (line != null) {
			sb.append(line).append("\n");
			line = buf.readLine();
		}

		String fileAsString = sb.toString();
		System.out.println("Contents : " + fileAsString);

		String[] asArray = fileAsString.split(",");
		List<String> ret = new ArrayList<>();
		for (String next : asArray) {
			ret.add(next.trim());
		}

		return ret;
	}

	private void execSearch(String term, String indexName, SearchRemoteWorker rsw, boolean doHash) {
		try {

			System.out.println("--- Local Execution");
			Timelapse stepTime = new Timelapse();
			stepTime.print(Messages.getString("DoSearch.9") + term); //$NON-NLS-1$
			SearchLocalWorker lsw = new SearchLocalWorkerImpl(searchLocalFactory);

			System.out.println(Messages.getString("DoSearch.7") + term + Messages.getString("DoSearch.8") + indexName); //$NON-NLS-1$ //$NON-NLS-2$
			if (doHash) {
				SearchCryptoService cryptoService = searchLocalFactory.getCryptoService();
				byte[] hashedTerm = cryptoService.calcSha256(term.getBytes());
				term = Converter.bytesToHex(hashedTerm);
			}
			stepTime.print("hashed term: " + term); //$NON-NLS-1$

			Set<String> encryptTerm = lsw.encryptTerm(term, indexName, false);
//			System.out.print("Terms to Search: ");
//			for (String next : encryptTerm) {
//				System.out.print(next + " ");
//			}
			System.out.println(Messages.getString("DoSearch.10")); //$NON-NLS-1$
			System.out.println(
					Messages.getString("DoSearch.11") + encryptTerm.size() + Messages.getString("DoSearch.12")); //$NON-NLS-1$ //$NON-NLS-2$
//			System.out.println(Messages.getString("DoSearch.13")+encryptTerm+Messages.getString("DoSearch.14")); //$NON-NLS-1$ //$NON-NLS-2$

			stepTime.print(Messages.getString("DoSearch.15") + encryptTerm.size() //$NON-NLS-1$
					+ Messages.getString("DoSearch.16")); //$NON-NLS-1$

			RemoteBucketManager bucket = searchRemoteFactory.getRemoteBucketManager();

			System.out.println("--- Remote Execution");
			System.out.println("Searching Encrypted Index");
			logEncryptTerm(encryptTerm);

//			List<String> hitsList = rsw.searchEncryptedIndex(indexName, encryptTerm, bucket);

			// -- local decryption of content after files found by number
//			stepTime.print(Messages.getString("DoSearch.17")); //$NON-NLS-1$
//			System.out.println("--- Local Execution");
//			decrypt(rsw, hitsList, indexName);
//			stepTime.print(Messages.getString("DoSearch.17")); //$NON-NLS-1$

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void logEncryptTerm(Set<String> encryptTerm) {

		try {
			String str = encryptTerm.toString();
			BufferedWriter writer = new BufferedWriter(new FileWriter("c:\\lixo\\terms\\encr-"+System.currentTimeMillis()+"-.txt"));
			writer.write(str);

			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void decrypt(SearchRemoteWorker rsw, List<String> hitsList, String indexName) throws Exception {
		System.out.println(Messages.getString("DoSearch.18")); //$NON-NLS-1$

		LocalBucketManager lb = cf.getLocalBucketManager();
		lb.deleteClearContent();
		RefByNamePersist refByNameStore = searchLocalFactory.getRefByNamePersist();
		Map<Integer, ContentRef> refByName = refByNameStore.load(indexName);
		for (String next : hitsList) {
			byte[] clearContent = loadClearContent(rsw, indexName, next, lb);

			Integer ndx = Integer.valueOf(next);
			ContentRef contentRef = refByName.get(ndx);
			byte[] sha256Orig = contentRef.getSha256();
			if (verifyIntegrity(sha256Orig, clearContent)) {

				lb.saveClearContent(contentRef, clearContent);
			} else {
				throw new Exception(Messages.getString("DoSearch.19") + ndx + Messages.getString("DoSearch.20") //$NON-NLS-1$ //$NON-NLS-2$
						+ contentRef.getName());
			}
		}

	}

	private boolean verifyIntegrity(byte[] sha256Orig, byte[] clearContent) throws NoSuchAlgorithmException {
		SearchCryptoService cryptoService = searchLocalFactory.getCryptoService();
		return cryptoService.verifyIntegrity(sha256Orig, clearContent);
	}

	private byte[] loadClearContent(SearchRemoteWorker rsw, String indexName, String next, LocalBucketManager lb)
			throws Exception {
		byte[] dataContent = rsw.loadEncryptedContent(next);
		byte[] clearContent = decryptContent(indexName, next, dataContent, lb);
		return clearContent;
	}

	private byte[] decryptContent(String indexName, String next, byte[] dataContent, LocalBucketManager lb)
			throws Exception {

		SearchCryptoService cryptoService = searchLocalFactory.getCryptoService();
		byte[] clearContent = cryptoService.decryptBlock(
				indexName + Messages.getString("DoSearch.21") + next + Messages.getString("DoSearch.22"), dataContent); //$NON-NLS-1$ //$NON-NLS-2$
		return clearContent;
	}
}
