package cryptSearch.service.commons.persist;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;

import cryptSearch.service.commons.content.LocalBucketManager;
import cryptSearch.service.commons.domain.ContentRef;

public class RefByNamePersistJson implements RefByNamePersist {
	private LocalBucketManager bucket;

	public RefByNamePersistJson(LocalBucketManager bucket) {
		super();
		this.bucket = bucket;
	}

//	public void init(LocalBucketManager bucket) {
//		this.bucket = bucket;
//		
//	}

	public void persist(Map<Integer, ContentRef> refByName, String name, boolean overwrite) throws Exception {
		if(!overwrite){
			Map<Integer, ContentRef> refByNameOld = load(name);
			refByName.putAll(refByNameOld);
		}
		File refByNameFile = bucket.getRefByNameFile(name);
		System.err.println("Persisting REF BY NAME: " +refByNameFile);
		File ksPath = refByNameFile .getParentFile();
		ksPath.mkdirs();

		OutputStream os = new FileOutputStream(refByNameFile);
		byte[] bytes = marshall(refByName).getBytes();
		os.write(bytes);
		os.close();
	}
	
	public Map<Integer, ContentRef> load(String name)  throws Exception {
		File refByNameFile = bucket.getRefByNameFile(name);
		System.err.println("Loading REF BY NAME: " +refByNameFile.getAbsolutePath());
		InputStream is = new FileInputStream(refByNameFile);
		byte[] b = new byte[is.available()];
		is.read(b);
		return unmarshall(new String(b));
	}
	
	private Map<Integer, ContentRef> unmarshall(String s){
		JSONObject jo = new JSONObject(s);
		Iterator<String> it = jo.keys();
		Map<Integer, ContentRef> ret = new HashMap<Integer, ContentRef>();
		while(it.hasNext()) {
			String keyStr = it.next();
			JSONObject value = jo.getJSONObject(keyStr);
			ContentRef contentRef = new Gson().fromJson(value.toString(), ContentRef.class);
			ret.put(Integer.parseInt(keyStr), contentRef);
			
		}
		return ret;
	}
	
	private String marshall(Map<Integer, ContentRef> refByName){
		return new JSONObject(refByName).toString();
	}

}
