package cryptSearch.lib.search.local.util;

import java.util.Arrays;

import cryptSearch.lib.search.local.util.RegexAnalysis.Usage;

public class RegexTips {

	private final RegexAnalysis.Usage[] usage;
	private final int minSize;
	private final int maxSize;
	
	public RegexTips(Usage[] usage, int minSize, int maxSize) {
		super();
		this.usage = usage;
		this.minSize = minSize;
		this.maxSize = maxSize;
	}

	private RegexAnalysis.Usage[] getUsage() {
		return usage;
	}

	private int getMinSize() {
		return minSize;
	}

	private int getMaxSize() {
		return maxSize;
	}
	
	@Override
	public String toString() {
		return "RegexTips [usage=" + Arrays.toString(usage) + ", minSize=" + minSize + ", maxSize=" + maxSize + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + maxSize;
		result = prime * result + minSize;
		result = prime * result + Arrays.hashCode(usage);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegexTips other = (RegexTips) obj;
		if (maxSize != other.maxSize)
			return false;
		if (minSize != other.minSize)
			return false;
		if (!Arrays.equals(usage, other.usage))
			return false;
		return true;
	}

}
