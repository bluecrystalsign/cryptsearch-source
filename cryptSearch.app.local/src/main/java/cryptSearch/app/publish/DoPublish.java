package cryptSearch.app.publish;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cryptSearch.lib.publish.local.factory.PublishLocalFactory;
import cryptSearch.lib.publish.local.factory.PublishLocalFactoryImpl;
import cryptSearch.lib.publish.local.worker.PublishLocalWorker;
import cryptSearch.lib.publish.local.worker.PublishLocalWorkerImpl;
import cryptSearch.service.commons.util.CacheHandler;
import cryptSearch.service.commons.util.CacheHandlerImpl;
import cryptSearch.service.commons.util.Timelapse;

public class DoPublish {

	private static final String PARM_HASH = "--hashed";

	public DoPublish() {
		super();
	}

	public static void main(String[] args) {
		System.out.println(Messages.getString("DoPublish.0")); //$NON-NLS-1$
		start(args);
		System.out.println(Messages.getString("DoPublish.1")); //$NON-NLS-1$

	}

	private static void start(String[] args) {
		if (args.length != 0) {
			List<String> behaviour = null;
			if (args.length > 1) {
				behaviour = Arrays.asList(Arrays.copyOfRange(args, 0, args.length));
			}
			try {
				String string = Messages.getString("DoPublish.2");
				String string2 = Messages.getString("DoPublish.3");
				if (behaviour != null && (behaviour.contains(string)
						|| behaviour.contains(string2))
						) { //$NON-NLS-1$ //$NON-NLS-2$
					new DoPublish().update(args);				
				} else {
					new DoPublish().create(args);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			printUsage(args);
		}
	}

	private void update(String[] args) throws Exception {
		System.out.println(Messages.getString("DoPublish.4")); //$NON-NLS-1$
		Timelapse totalTime = new Timelapse();
		if (invalidParemsUpdate(args)) {
			printUsage(args);
			return;
		}
		
		String propFilter = Config.getString("DoPublish.1"); //$NON-NLS-1$
		System.out.println("File types: "+propFilter);

		String[] filters = propFilter.split(Messages.getString("DoPublish.6")); //$NON-NLS-1$
		String[] initialPath = { args[args.length - 1] };

		String indexName = args[1];

		PublishLocalFactory plf = PublishLocalFactoryImpl.getInstance();
		CacheHandler cacheHandler = new CacheHandlerImpl(plf.getCachePersist(), plf.getLocalBucketManager(), false);

		PublishLocalWorker plw = new PublishLocalWorkerImpl(plf, initialPath, filters, cacheHandler, false);
		Map<String, Set<String>> tokensToIndex = plw.createEncryptedTokensToUpdate(indexName);
		
		totalTime.print(Messages.getString("DoPublish.7")); //$NON-NLS-1$

	}

	private void create(String[] args) throws Exception {
		Timelapse totalTime = new Timelapse();
		if (invalidParemsCreate(args)) {
			printUsage(args);
			return;
		}

		String propFilter = Config.getString("DoPublish.1"); //$NON-NLS-1$
		String[] filters = propFilter.split(Messages.getString("DoPublish.9")); //$NON-NLS-1$
		
		String[] initialPath = { args[args.length - 1] };

		String indexName = args[args.length - 2];
		List<String> behaviour  = null;
		if(args.length > 2) {
			behaviour  = Arrays.asList(Arrays.copyOfRange(args, 0, args.length - 2));
		}
		
		
		System.out.println(Messages.getString("DoPublish.10")+indexName); //$NON-NLS-1$
		System.out.println(Messages.getString("DoPublish.11")+Arrays.toString(initialPath)); //$NON-NLS-1$

		PublishLocalFactory plf = PublishLocalFactoryImpl.getInstance();

		boolean doHash = (behaviour != null && behaviour.contains(PARM_HASH));
		PublishLocalWorker plw = new PublishLocalWorkerImpl(plf, initialPath, filters, null,doHash);
		plw.createEncryptedTokens(indexName);
		totalTime.print(Messages.getString("DoPublish.13")); //$NON-NLS-1$

	}

	private boolean invalidParemsCreate(String[] args) {
		if (args.length >= 2) {
			File f = new File(args[args.length - 1]);
			return !(f.exists() && f.isDirectory() && f.canRead());
		}
		return true;
	}

	private boolean invalidParemsUpdate(String[] args) {
		if (args.length >= 3) {
			File f = new File(args[args.length - 1]);
			return !(f.exists() && f.isDirectory() && f.canRead());
		}
		return true;
	}

	private static void printUsage(String[] args) {
		System.out.println(Messages.getString("DoPublish.14")); //$NON-NLS-1$
		System.out.println(
				Messages.getString("DoPublish.15")); //$NON-NLS-1$
		System.out.println(
				Messages.getString("DoPublish.16")); //$NON-NLS-1$
		for (String next : args) {
			System.out.println(Messages.getString("DoPublish.17") + next); //$NON-NLS-1$
		}
	}

}
