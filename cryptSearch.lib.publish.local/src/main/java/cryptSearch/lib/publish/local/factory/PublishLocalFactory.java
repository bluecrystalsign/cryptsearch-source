package cryptSearch.lib.publish.local.factory;

import cryptSearch.lib.publish.local.domain.ContentDiscovery;
import cryptSearch.lib.publish.local.message.ContentDiscoveryWork;
import cryptSearch.lib.publish.local.util.ContentLoader;
import cryptSearch.lib.publish.local.util.PublishCryptoService;
import cryptSearch.lib.publish.local.util.Tokenizer;
import cryptSearch.service.commons.content.LocalBucketManager;
import cryptSearch.service.commons.persist.CachePersist;
import cryptSearch.service.commons.persist.RefByNamePersist;
import cryptSearch.service.commons.persist.TokenListStore;
import cryptSearch.service.commons.util.CommonsFactory;

public interface PublishLocalFactory {
	public ContentLoader getContentLoader();
	public Tokenizer getTokenizer();
	public ContentDiscovery getContentDiscovery(String initialPath[], String[] filters);
	public RefByNamePersist getRefByNamePersist();
	public CachePersist getCachePersist();
	public ContentDiscoveryWork getContentDiscoveryWork(String path, String[] filter);
	public PublishCryptoService getCryptoService(); 
//	public CommonsFactory getCommonsFactory();
	public LocalBucketManager getLocalBucketManager();
	public TokenListStore getTokenList(String filePath);
	public void initRemoteBucketManager(boolean delete);

}