package cryptSearch.lib.publish.remote.factory;

import cryptSearch.lib.publish.remote.util.IndexPublisherLucene;
import cryptSearch.lib.publish.remote.util.RemoteIndexPublisher;
import cryptSearch.service.commons.content.RemoteBucketManager;
import cryptSearch.service.commons.content.RemoteBucketManagerLocalFS;
import cryptSearch.service.commons.persist.TokenListStore;
import cryptSearch.service.commons.util.CommonsFactoryImpl;

public class PublishRemoteFactoryImpl implements PublishRemoteFactory {
	
	private static PublishRemoteFactoryImpl factory;
	private PublishRemoteFactoryImpl() {
	}
	public static PublishRemoteFactory getInstance(){
		if(factory == null){
			factory = new PublishRemoteFactoryImpl();
		}
		return factory;
	}
	public RemoteIndexPublisher getIndexer(String indexName, 
			RemoteBucketManager remoteBucketManager, 
			boolean recreate) throws Exception {
		return new IndexPublisherLucene(indexName, remoteBucketManager, recreate);
	}

	public RemoteBucketManager getRemoteBucketManager() {
		return RemoteBucketManagerLocalFS.getInstance();
	}
	
	@Override
	public TokenListStore getTokenList(String filePath) {
//		return CommonsFactoryImpl.getInstance().getTokenList(filePath);
		
		TokenListStore tokenList = CommonsFactoryImpl.getInstance().getTokenList(filePath);
		tokenList.init(getRemoteBucketManager());
		return tokenList;
	}
	

}
