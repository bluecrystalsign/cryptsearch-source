package cryptSearch.lib.publish.remote.factory;

import cryptSearch.lib.publish.remote.util.RemoteIndexPublisher;
import cryptSearch.service.commons.content.RemoteBucketManager;
import cryptSearch.service.commons.persist.TokenListStore;

public interface PublishRemoteFactory {

	public RemoteIndexPublisher getIndexer(String indexName, 
			RemoteBucketManager remoteBucketManager, 
			boolean recreate) throws Exception;
	public RemoteBucketManager getRemoteBucketManager();
	TokenListStore getTokenList(String filePath);
}