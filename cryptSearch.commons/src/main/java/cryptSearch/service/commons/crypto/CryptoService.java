package cryptSearch.service.commons.crypto;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Map;
import java.util.Set;

import javax.crypto.NoSuchPaddingException;

public interface CryptoService {

	CipherKey createCihperKeyBlock(String string) throws Exception;
	CipherKey createCihperKeyStream(String indexName) throws Exception;
	CipherKey loadStreamKeyStore(String indexName) throws Exception, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException;

	byte[] loadEncryptPersistContent(String nextPath, CipherKey cbcCipherKey, int i) throws Exception;


	public int getNextContentIndex() throws Exception;
	Map<String, String> encryptIndex(Map<Integer, Set<String>> tokensByLength,
			CipherKey ctrCipherKey, int prefixLength) throws Exception;

	
	byte[] encryptBlock(byte[] bytes, CipherKey ck) throws Exception;
	public byte[] decryptBlock(String ksname, byte[] content) throws Exception;
	public byte[] calcSha256(byte[] content) throws NoSuchAlgorithmException;
	boolean verifyIntegrity(byte[] sha256Orig, byte[] clearContent) throws NoSuchAlgorithmException;
	String bytesToHex(byte[] bytes);
	
}
