package cryptSearch.service.commons.util;

import cryptSearch.service.commons.content.LocalBucketManager;
import cryptSearch.service.commons.content.RemoteBucketManager;
import cryptSearch.service.commons.crypto.CryptoService;
import cryptSearch.service.commons.persist.CachePersist;
import cryptSearch.service.commons.persist.RefByNamePersist;
import cryptSearch.service.commons.persist.TokenListStore;

public interface CommonsFactory {

	RemoteBucketManager getRemoteBucketManager();

	LocalBucketManager getLocalBucketManager();
	public CryptoService getCryptoService();

	TokenListStore getTokenList(String filePath);

	CachePersist getCachePersist();

	RefByNamePersist getRefByNamePersist();
}