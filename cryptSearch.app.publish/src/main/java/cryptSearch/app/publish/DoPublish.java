package cryptSearch.app.publish;

import java.io.File;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

import cryptSearch.lib.publish.local.util.PublishLocalFactory;
import cryptSearch.lib.publish.local.util.PublishLocalFactoryImpl;
import cryptSearch.lib.publish.local.worker.PublishLocalWorker;
import cryptSearch.lib.publish.local.worker.PublishLocalWorkerImpl;
import cryptSearch.lib.publish.remote.util.PublishRemoteFactory;
import cryptSearch.lib.publish.remote.util.PublishRemoteFactoryImpl;
import cryptSearch.lib.publish.remote.worker.PublishRemoteWorker;
import cryptSearch.lib.publish.remote.worker.PublishRemoteWorkerImpl;
import cryptSearch.service.commons.util.CacheHandler;
import cryptSearch.service.commons.util.CacheHandlerImpl;
import cryptSearch.service.commons.util.Timelapse;

public class DoPublish {

	public DoPublish() {
		super();
	}

	public static void main(String[] args) {
		System.out.println("*** Start");
		start(args);
		System.out.println("*** Finish");

	}

	private static void start(String[] args) {
		if (args.length != 0) {
			try {
				if (args[0].compareTo("--update") == 0 || args[0].compareTo("-u") == 0) {
					new DoPublish().update(args);
				} else {
					new DoPublish().create(args);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			printUsage(args);
		}
	}

	private void update(String[] args) throws Exception {
		System.out.println("Updating index");
		Timelapse totalTime = new Timelapse();
		if (invalidParemsUpdate(args)) {
			printUsage(args);
			return;
		}

		String[] filters = { "pdf", "doc", "xls", "ppt", "docx" };
		String[] initialPath = { args[args.length - 1] };

		String indexName = args[1];

		PublishLocalFactory plf = PublishLocalFactoryImpl.getInstance();
		CacheHandler cacheHandler = new CacheHandlerImpl(plf.getCachePersist(), plf.getLocalBucketManager(), false);

		PublishLocalWorker plw = new PublishLocalWorkerImpl(plf, initialPath, filters, cacheHandler);
		Map<String, Set<String>> tokensToIndex = plw.createEncryptedTokensToUpdate(indexName);

		PublishRemoteFactory prf = PublishRemoteFactoryImpl.getInstance();
		PublishRemoteWorker prw = new PublishRemoteWorkerImpl(prf);
		prw.initIndex(indexName, false);
		for (String contentRefence : tokensToIndex.keySet()) {
			prw.updateIndex(contentRefence, tokensToIndex.get(contentRefence));
		}

		prw.finish();

		totalTime.print("Total execution time");

	}

	private void create(String[] args) throws Exception {
		Timelapse totalTime = new Timelapse();
		if (invalidParemsCreate(args)) {
			printUsage(args);
			return;
		}
		String[] filters = { "pdf", "doc", "xls", "ppt", "docx" };
		String[] initialPath = { args[args.length - 1] };

		String indexName = args[0];
		
		System.out.println("Index name: "+indexName);
		System.out.println("Path to index: "+Arrays.toString(initialPath));

		PublishLocalFactory plf = PublishLocalFactoryImpl.getInstance();

		PublishLocalWorker plw = new PublishLocalWorkerImpl(plf, initialPath, filters, null);
		Map<String, Set<String>> tokensToIndex = plw.createEncryptedTokens(indexName);

		PublishRemoteFactory prf = PublishRemoteFactoryImpl.getInstance();
		PublishRemoteWorker prw = new PublishRemoteWorkerImpl(prf);
		prw.initIndex(indexName, true);
		for (String contentRefence : tokensToIndex.keySet()) {
			prw.addDocToIndex(contentRefence, tokensToIndex.get(contentRefence));
		}

		prw.finish();

		totalTime.print("Total time");

	}

	private boolean invalidParemsCreate(String[] args) {
		if (args.length >= 2) {
			File f = new File(args[args.length - 1]);
			return !(f.exists() && f.isDirectory() && f.canRead());
		}
		return true;
	}

	private boolean invalidParemsUpdate(String[] args) {
		if (args.length >= 3) {
			File f = new File(args[args.length - 1]);
			return !(f.exists() && f.isDirectory() && f.canRead());
		}
		return true;
	}

	private static void printUsage(String[] args) {
		System.out.println("Exec from command line");
		System.out.println(
				"Creating a new index - doPublish <index name> <path of content to index>");
		System.out.println(
				"Updating an index - doPublish --update | -u <index name> <path of content to index>");
		for (String next : args) {
			System.out.println(">> " + next);
		}

	}

}
