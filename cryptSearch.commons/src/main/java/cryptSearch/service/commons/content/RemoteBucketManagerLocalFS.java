package cryptSearch.service.commons.content;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import cryptSearch.service.commons.crypto.Messages;

public class RemoteBucketManagerLocalFS implements RemoteBucketManager {
	private static final int MAX_DATA_FILE = 100 * 1000;
	private final String TEMP_PATH = System.getProperty("user.home") + File.separator + "cryptsearch" + File.separator + "temp"; //$NON-NLS-1$
	private final String BASE_PATH = System.getProperty("user.home") + File.separator + "cryptsearch" + File.separator + "remote"; //$NON-NLS-1$
	private final String DATA_PATH = "dados"; //$NON-NLS-1$
	private final String INDEX_PATH = "index"; //$NON-NLS-1$
	private final String TOKEN_LIST_PATH = "encryptedTokenByDocument"; //$NON-NLS-1$
	private final String EXTENSION = ".json";
	
	private static RemoteBucketManager instance;
	
	
	
	private RemoteBucketManagerLocalFS() {
		init(false);
	}
	
	public static RemoteBucketManager getInstance() {
		if(instance == null) {
			instance = new RemoteBucketManagerLocalFS();
		}
		return instance;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cryptSearch.service.commons.content.BucketManager#getDataPath()
	 */
	private File getDataPath() {
		return new File(BASE_PATH + File.separator + DATA_PATH);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cryptSearch.service.commons.content.BucketManager#getDataFile(java.lang
	 * .String)
	 */
	public File getDataFile(String filePath) {
		return new File(BASE_PATH + File.separator + DATA_PATH + File.separator
				+ filePath);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cryptSearch.service.commons.content.BucketManager#getIndexPath()
	 */
	private File getIndexPath() {
		return new File(BASE_PATH + File.separator + INDEX_PATH);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cryptSearch.service.commons.content.BucketManager#getIndexPath(java.lang
	 * .String)
	 */
	public File getIndexPath(String indexName) {
		File file = new File(BASE_PATH + File.separator + INDEX_PATH
				+ File.separator + indexName);
		System.err.println("Get Index Path: "+file);
		return file;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cryptSearch.service.commons.content.BucketManager#clearIndexPath(java
	 * .lang.String)
	 */
	public File clearIndexPath(String indexName) {
		File f = getIndexPath(indexName);
		if (f.exists()) {
			deleteDirectory(f);
		}
		f.mkdirs();
		System.err.println("Clear Index Path: "+f);
		return f;
	}

	public File loadIndexPath(String indexName) {
		File f = getIndexPath(indexName);
		System.err.println("Load Index Path: "+f);
		return f;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cryptSearch.service.commons.content.BucketManager#deleteDirectory(java
	 * .io.File)
	 */
	public boolean deleteDirectory(File path) {
		if (path.exists()) {
			File[] files = path.listFiles();
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					deleteDirectory(files[i]);
				} else {
					files[i].delete();
				}
			}
		}
		return (path.delete());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cryptSearch.service.commons.content.BucketManager#createDirs()
	 */
	public void init(boolean delete) {
		if (getDataPath().exists()) {
			if (delete) {
				deleteDirectory(getDataPath());
			}
			getDataPath().mkdirs();
		}
		if (getIndexPath().exists()) {
			if (delete) {
				deleteDirectory(getIndexPath());
			}
			getIndexPath().mkdirs();
		}
		if (!getTokenListPath().exists()) {
			getTokenListPath().mkdirs();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cryptSearch.service.commons.content.BucketManager#loadEncryptedContent
	 * (java.lang.String)
	 */
	public byte[] loadEncryptedContent(String next) throws Exception {
		File dataFile = getDataFile(next);
		byte[] dataContent = loadContent(dataFile);
		System.out.println("De: " + dataFile.getAbsolutePath());
		return dataContent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cryptSearch.service.commons.content.BucketManager#loadContent(java.io
	 * .File)
	 */
	public byte[] loadContent(File dest) throws Exception {
		InputStream is = new FileInputStream(dest);
		byte[] b = new byte[is.available()];
		is.read(b);
		is.close();
		return b;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cryptSearch.service.commons.content.BucketManager#storeEncrypted(int,
	 * byte[])
	 */
	public void storeEncrypted(int i, byte[] encContent)
			throws FileNotFoundException, IOException {
		String name = getDataPath() + File.separator+ i;
		File ksFile = new File(name);
		File ksPath = ksFile .getParentFile();
		ksPath.mkdirs();

		System.err.println("Persisting ENCRYPTED CONTENT: " +name);
		OutputStream os = new FileOutputStream(name);
		os.write(encContent);
		os.close();
	}

	public int getNextContentIndex() {
		for (int i = 0; i < MAX_DATA_FILE; i++) {
			File f = new File(getDataPath() + File.separator + i);
			if (!f.exists()) {
				return i;
			}
		}
		return 0;

	}

		@Override
		public File getTokenListFile(String filePath) {
			return new File(getTokenListPath() + File.separator
					+ filePath+ EXTENSION);
		}

		private File getTokenListPath() {
			String pathname = TEMP_PATH + File.separator + TOKEN_LIST_PATH;
			return new File(pathname);
		}
	
}
