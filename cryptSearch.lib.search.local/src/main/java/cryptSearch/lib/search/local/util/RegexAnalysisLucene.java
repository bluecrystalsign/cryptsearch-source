package cryptSearch.lib.search.local.util;

import java.util.HashMap;
import java.util.Map;

public class RegexAnalysisLucene implements RegexAnalysis {
	
	private static final int NOT_FOUND = -1;
	private boolean supportOptional;
	
	Map<RegexAnalysis.Usage, String> special = new HashMap<>(); 
	Map<String, RegexAnalysis.Usage> specialRev = new HashMap<>(); 

//	Any Unicode characters may be used in the pattern, but certain characters are reserved and must be escaped. The standard reserved characters are:
//
//		. ? + * | { } [ ] ( ) " \
//
//		If you enable optional features (see below) then these characters may also be reserved:
//
//		@ &   ~
	
	
	
	final char[] reserved = {'.', '?', '+', '*', '|', '{', '}', '[', ']', '(', ')', '"', '\\'};
	final char[] optional = {'@', '&', '~'};
	
	public RegexAnalysisLucene(boolean supportOptional) {
		super();
		this.supportOptional = supportOptional;
	
		special.put(RegexAnalysis.Usage.ESCAPE, "\\");
		special.put(RegexAnalysis.Usage.ANY_CHAR_ONCE, ".");
		special.put(RegexAnalysis.Usage.PRECEDING_ONE_PLUS, "+");
		special.put(RegexAnalysis.Usage.PRECEDING_ZERO_PLUS, "*");
		special.put(RegexAnalysis.Usage.PRECEDING_OPTIONAL, "?");
		special.put(RegexAnalysis.Usage.START_REPEAT_COUNT, "{");
		special.put(RegexAnalysis.Usage.END_REPEAT_COUNT, "}");
	
		for(RegexAnalysis.Usage next : special.keySet()) {
			specialRev.put(special.get(next), next);
		}
	
	}

	@Override
	public RegexTips analiseRegex(String original) {

		RegexAnalysis.Usage[] usage = new RegexAnalysis.Usage[original.length()];
		int minSize = 0;
		int maxSize = RegexAnalysis.UNLIMITED;
		
		for(int i  = 0; i < original.length(); i++ ) {
			String s = original.substring(i, i+1);
			if(specialRev.containsKey(s)) {
				usage[i] = specialRev.get(s);
			}
		}

		
		return new RegexTips(usage, minSize, maxSize);
	}

}
