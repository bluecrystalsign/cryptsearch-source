package cryptSearch.service.commons.persist;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.json.JSONObject;

import com.google.gson.Gson;

import cryptSearch.service.commons.content.LocalBucketManager;
import cryptSearch.service.commons.crypto.KeyStore;
import cryptSearch.service.commons.crypto.KeyStoreImpl;

public class KeyPersistJson implements KeyPersistStore {
	private LocalBucketManager bucket;

	public void init(LocalBucketManager bucket){
		this.bucket = bucket;
	}
	
	private String marshall(KeyStore ks){
		JSONObject ja = new JSONObject(ks);
		return ja.toString();
	}
	
	private KeyStore unmarshall(String s){
		JSONObject jo = new JSONObject(s);
		KeyStore keyStore = new Gson().fromJson(jo.toString(), KeyStoreImpl.class);
		return keyStore;
	}

	public void persist(KeyStore ks, String filePath) throws Exception {
		File ksFile = bucket.getKsFile(filePath);
		System.err.println("Persisting KEY: " +ksFile);
		File ksPath = ksFile.getParentFile();
		ksPath.mkdirs();
		OutputStream os = new FileOutputStream(ksFile);
		os.write(marshall(ks).getBytes());
		os.close();
		
	}

	public KeyStore load(String filePath)  throws Exception {
		
		File ksFile = bucket.getKsFile(filePath);
		System.err.println("Loading KEY: " +ksFile);
		InputStream is = new FileInputStream(ksFile.getAbsolutePath());
		byte[] b = new byte[is.available()];
		is.read(b);
		return unmarshall(new String(b));
	}
}
