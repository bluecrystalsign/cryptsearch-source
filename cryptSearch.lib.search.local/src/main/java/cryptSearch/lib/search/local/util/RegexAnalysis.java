package cryptSearch.lib.search.local.util;

public interface RegexAnalysis {
	

	public enum Usage {
		ESCAPE, 				// \ 
		ANY_CHAR_ONCE, 			// . 
		PRECEDING_ONE_PLUS,  	// +   	the plus sign is the match-one-or-more quantifier
		PRECEDING_ZERO_PLUS,  	// *	the asterisk is the match-zero-or-more quantifier
		PRECEDING_OPTIONAL,		// ?   	the question mark is the match-zero-or-one quantifier
		START_REPEAT_COUNT,		// {
		END_REPEAT_COUNT		// }
	}
	
	public final int UNLIMITED = -1;

	RegexTips analiseRegex(String original);

	// examples (cryptography) Match
	// cryptography
	// crypto.*			- matches olny crypto
	// crypto.+			- starts with crypto
	// crypto.*phy		- starts with crypto and ends with phy
	// cryptograph.
	// cryptograp..
	// cryptograp.{2}
	// crypto..aphy
	// crypto.?.?aphy
	// crypto.{2}aphy
	// crypto...?phy
	// crypto...+phy
	// crypto....?phy
	// crypto...phy
	
	// The .* operators match any characters of any length, including no characters. 
	// The performance of the regexp query can vary based on the regular expression provided. To improve performance, avoid using wildcard patterns, such as .* or .*?+, without a prefix or suffix.

	
}