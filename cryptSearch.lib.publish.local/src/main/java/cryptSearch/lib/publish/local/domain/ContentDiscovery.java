package cryptSearch.lib.publish.local.domain;

import java.util.List;

import cryptSearch.lib.publish.local.factory.PublishLocalFactory;
import cryptSearch.lib.publish.local.message.ContentUri;

public interface ContentDiscovery {
	public List<ContentUri> discoverFiles(PublishLocalFactory plf);
}
